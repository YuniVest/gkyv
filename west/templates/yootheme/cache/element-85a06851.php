<?php // @file /var/www/html/gkyv/west/templates/yootheme/vendor/yootheme/builder/elements/layout/element.json

return [
  'name' => 'layout',
  'title' => 'Layout',
  'container' => true,
  'templates' => [
    'render' => "{$file['dirname']}/templates/template.php",
    'content' => "{$file['dirname']}/templates/template.php"
  ]
];
