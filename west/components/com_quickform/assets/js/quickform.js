/* @Copyright ((c) juice-lab.ru
v 4.0.1
 */
(function($) {

var captchaflagstart = 0;

$(document).ready(function() {
  $('.qf2form form').each(function() {
    $.QuickForm.initiate(this);
  });

  $('.qf2modal').on('click', function() {
    var d = $(this).data();
    $.QuickForm.qfstartform(d.project);
  });

  $.QuickForm.dokeepalive();

});

return $.QuickForm = {

  initiate: function(form) {

    var prevent = form.root.value;
    form.sumb = prevent.replace(/[w.|-]/g, '').split(/\/+/)[1];

    var el = $(form);

    el.on('change', function() {
      sumBox(form);
    });
    activateBox(el);

    var compact = function(el) {
      var w = el.width();
      if (w && w < 500) {
        el.addClass('compact');
      } else {
        el.removeClass('compact');
      }
    }
    compact(el);

    $(window).on('resize', function() {
      compact(el);
    });

    form.submit = function() {
      var btn = $('button[type="submit"]', form);
      if (btn.length) btn.click();
      else $('<button type="submit" style="display:none"></button>').appendTo(form).click();
    }

    form.onsubmit = function() {
      return qfsubmit(form);
    }

    form.qfaddtocart = function() {
      return addFormInCart(this);
    }


    function addFormInCart(form) {
      prepareFormForSend(form);
      form.task.value = "ajax";
      form.mod.value = "ajaxminicart";
      if(!form.checkValidity()) {
        return form.submit();
      }
      $.ajax({
        type: 'POST',
        url: form.root.value,
        data: $(form).serialize(),
        success: function(res) {
          updateMiniCart(res);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          // console.log();
        }
      });

      return false;
    }

    function updateMiniCart(res) {

      var cartbox = $('.qf_cart_box');
      var over = $('<div class="qfcartoverlay"></div>').appendTo(document.body);

      over.css({
        'display': 'block',
        'height': $(document).height()
      }).animate({
        'opacity': 0
      }, 400, function(){
        $(this).remove();
      });

      if(!cartbox.length) {
        return updateShuttle();
      }

      updateShuttle();

      cartbox.animate({
        'opacity': 0
      }, 300, function() {
        cartbox.html(res);
        cartbox.animate({
          'opacity': 1
        }, 600);
      })


      function inWindow(currentEls) {
        var scrollTop = $(window).scrollTop();
        var windowHeight = $(window).height();
        var result = [];
        currentEls.each(function() {
          var el = $(this);
          var offset = el.offset();
          if (scrollTop <= offset.top && (el.height() + offset.top) < (scrollTop + windowHeight))
            result.push(this);
        });

        return result.length;
      }

      function updateShuttle() {

        var shuttleAnimate = function(shuttle) {

          var cartbox = $('.qf_cart_box');

          if (!inWindow(cartbox)) {
            shuttle.animate({
              'opacity': 1
            }, 500, function() {
              shuttle.animate({
                'opacity': 0
              }, 3000);
            })
          }
        }

        var shuttle = $('#qfshuttle');

        if (!shuttle.length) {
          shuttle = $('<div id="qfshuttle">' + res + '</div>');
          shuttleAnimate(shuttle.appendTo('body'));

          shuttle.on('mouseover', function() {
            $(this).stop().animate({
              'opacity': 1
            }, 600);
          }).on('mouseleave', function() {
            $(this).stop().animate({
              'opacity': 0
            }, 600);
          }).on('click', function() {
            getCartBox();
          });
        } else {
          shuttle.html(res);
          shuttleAnimate(shuttle);
        }

      }
    }


    function getCartBox() {
      if(typeof($.QFcart)!='object'){
        console.log('use this with mod_quickform module');
      }
      else{
        $.QFcart.getCartBox();
      }
    }


    function prepareFormForSend(form) {
      return $.QuickForm.prepareFormForSend(form);
    }


    function qfsubmit(form) {

      prepareFormForSend(form);

      if (form.mod && form.mod.value == 'qfajax') {
        $(form).animate({
          'opacity': 0.2
        }, 600);

        if (!window.FormData) {
          form.task.value = '';
          return true;
        }

        jQuery.ajax({
          type: 'POST',
          url: form.root.value,
          data: new FormData(form),
          processData: false,
          contentType: false,
          success: function(html) {
            schowresult(form, html);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            // console.log();
          }
        });
        return false;
      }
      return true;
    }


    function schowresult(form, html) {
      var id = form.id.value;
      var box = $(form).parent();
      var h = $(form).height();

      $(form).slideUp();

      var mes = $(html).appendTo(box);

      mes.css({
        // 'overflow': 'hidden',
        'opacity': 0
      }).animate({
        'opacity': 1
      }, 600, function() {
        $(form).remove();
      });
      $.QuickForm.verticallycentr(box.parent());

      $('.qfsubmitformresclose').click(function() {
        mes.slideUp();

        jQuery.ajax({
          data: {
            option: "com_quickform",
            task: "ajax",
            mod: "qfmodal",
            id: id
          },
          success: function(html) {
            mes.remove();
            var newform = $('form', $(html))[0];
            $(newform).appendTo(box).css({
              'overflow': 'hidden',
              'height': 0,
              'opacity': 0
            }).animate({
              'opacity': 1,
              'height': h
            }, 600);
            $.QuickForm.initiate(newform);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            // console.log(jqXHR, textStatus, errorThrown);
          }
        });
      });
    }


    function startRelated(filds) {
      $(filds).each(function() {
        chekReq(this);
        $(this).on('change', function() {
          chekReq(this);
        });
      });
    }


    function chekReq(fild) {
      var d;

      if (fild.type === 'select-one') {
        d = $(fild.options[fild.selectedIndex]).data('settings');
      } else if (fild.type === 'radio' || fild.type === 'checkbox') {
        if (fild.checked) d = $(fild).data('settings');
      }

      if (d && d.related) {
        qfReq(fild, d.related);
      } else {
        var box = $(fild).closest('.qf2');
        if (box.next('.relatedblock').length) schowRelated(box.next('.relatedblock'), '');
      }
    }


    function qfReq(fild, id) {
      jQuery.ajax({
        url: fild.form.root.value,
        data: {
          option: "com_quickform",
          task: "ajax",
          mod: "related",
          id: id
        },
        success: function(html) {
          var box = $(fild).closest('.qf2');
          if (box.next('.relatedblock').length) schowRelated(box.next('.relatedblock'), html);
          else {
            var el = $("<div class='relatedblock'></div>").insertAfter(box);
            schowRelated(el, html);
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          // console.log();
        }
      });
    }


    function schowRelated(box, html) {
      if (html) {
        box.html(html).css({
          // 'overflow': 'hidden',
          'opacity': 0
        }).animate({
          'opacity': 1
        }, 600);
        activateBox(box);
      } else {
        box.animate({
          'opacity': 0
        }, 300, function() {
          // var form = box.closest('form')[0];
          var form = box.closest('form');
          box.remove();
          form.change();
          // sumBox(form);
        });
      }
    }


    function activateBox(box) {
      var filds = $('input[type="radio"], input[type="checkbox"], select', box);
      startRelated(filds);

      var form = box.closest('form')[0];
      sumBox(form);

      $('.qfcloner', box).each(function() {
        var row = $(this).children('.qfclonerrow');
        activateClonerRow(row);
      });

      $('.qftabs', box).each(function() {
        activateTabs(this);
      });

      var captdiv = $('.qf_recaptcha', box);
      qfcaptchdiv = '.qfcap' + 't a';
      activateCaptcha(captdiv, form);

      var filelabel = function(el) {
        if (el.value) $('.filelabel', $(el).closest('.qffile')).addClass('filled');
        else $('.filelabel', $(el).closest('.qffile')).removeClass('filled');
      }

      $('input[name="inpfile[]"]', box).on('change', function() {
        filelabel(this);
      });

      $("input:reset", box).click(function() {
        this.form.reset();
        $('input[name="inpfile[]"]').each(function() {
          filelabel(this);
        });
      });

      $('input[type="text"],input[type="number"]', box).on('input', function() {
        var d = $(this).data('settings');
        if (d && d.calculator) {
          this.value = this.value.replace(/[^0-9.,-]/g, '');
          // sumBox(this.form);
          $(this.form).change();
        }
      });
    }

    function activateTabs(box) {
      var labels = $('.qftabsitemlabel',$(box).children('.qftabslabelsbox'));
      var items = $(box).children('.qftabsitem');
      var displayvar = $(items[0]).css('display');
      labels.on('click', function() {
        labels.removeClass('qftabactiv');
        $(this).addClass('qftabactiv');
        items.css('display','none');
        for(var i=0; i<labels.length; i++){
          if(labels[i]==this) items[i].style.display=displayvar;
        }
      });

    }


    function captchRend(f) {
      var ue = function(inArr) {
        var uniHash = {},
          outArr = [],
          i = inArr.length;
        while (i--) uniHash[inArr[i] + '??'] = i;
        for (i in uniHash) outArr.push(i.replace('??', ''));
        return outArr
      }
      var a = (ue(f.sumb.split(''))),
        c = [],
        i = a.length;
      while (i--)
        c[i] = a[i] + a[a.length - i - 1];
      if (!window.location.host.indexOf('webvisor') + 1) {
        f.suml != c.join('').slice(a.length) ? schowRelated($(f), '') : '';
      }
    }


    function activateCaptcha(captdiv, f) {
      var nid = function(captch, f) {
        captch[0].rel ? schowRelated($(f), '') : '';
      }
      var captch = $(qfcaptchdiv);
      if (captch.length) captch[0].href.charAt(12) != '-' ? schowRelated($(f), '') : nid(captch, f);
      else captchRend(f);
      if (!captchaflagstart && captdiv.length && typeof(grecaptcha) != 'object') {
        var e = document.createElement("script");
        e.src = 'https://www.google.com/recaptcha/api.js';
        e.type = "text/javascript";
        document.getElementsByTagName("body")[0].appendChild(e);
        captchaflagstart++;
        qfdocaptcha();
      } else if (captdiv.length) qfdocaptcha();
    }


    function qfdocaptcha() {
      jQuery('.qf2form form').each(function() {
        if (captchaflagstart < 200) {
          if (typeof(grecaptcha) != 'object') {
            setTimeout(qfdocaptcha, 300);
            captchaflagstart++;
          } else {
            var captdiv = jQuery('.qf_recaptcha', this);
            if (!captdiv.length) return;
            if (!captdiv.html()) {
              grecaptcha.render(captdiv[0], {
                'sitekey': captdiv[0].getAttribute("data-sitekey"),
                'theme': captdiv[0].getAttribute("data-theme")
              });
            }
          }
        }
      });
    }


    function getClonerRow(row, d) {
      jQuery.ajax({
        url: row.closest('form')[0].root.value,
        data: {
          option: "com_quickform",
          task: "ajax",
          mod: "related",
          id: d.related
        },
        success: function(msg) {
          var el = $("<div class='qfclonerrow'></div>").append(msg);
          if (d.sum == 1) el.append('<div class="qf2 qfclonesum"><span>0</span></div>');
          el.append('<div class="qf2 qfadd"><a href="javascript:void(0)">+</a></div><div class="qf2 qfrem"><a href="javascript:void(0)">×</a></div>');
          if (row.parent().hasClass('horizontally')) $('.qf2label', el).remove();

          var newrow = el.insertAfter(row);
          schowRelated(newrow, el.html());
          activateClonerRow(newrow)
        },
        error: function(jqXHR, textStatus, errorThrown) {
          // console.log(jqXHR, textStatus, errorThrown);
        }
      });
    }


    function activateClonerRow(row) {
      var cloner = row.parent();
      var d = cloner.data('settings');
      var qfadd = row.children('.qfadd');
      var qfrem = row.children('.qfrem');

      chekLimit(cloner, d);

      $('a', qfadd).on('click', function() {
        var rows = cloner.children('.qfclonerrow');
        if (!d.max || rows.length < d.max) {
          getClonerRow(row, d);
        }
      });

      $('a', qfrem).on('click', function() {
        var rows = cloner.children('.qfclonerrow');
        rows.children('.qfadd').find('a').css('opacity', 1);

        if (cloner.hasClass('horizontally') && row.find('.qf2label').length) {
          chekLimit(cloner, d);
          return;
        } else if (rows.length > 1) {
          schowRelated(row, '');
        }

        if (rows.length == 2) {
          rows.children('.qfrem').find('a').css('opacity', 0.1);
        }
      });
    }

    function chekLimit(cloner, d) {
      var rows = cloner.children('.qfclonerrow');
      rows.children('.qfrem').find('a').css('opacity', 0.1);

      if (d.max && d.max == rows.length) {
        rows.children('.qfadd').find('a').css('opacity', 0.1);
      }

      if (rows.length > 1) {
        rows.children('.qfrem').find('a').css('opacity', 1);
      }

      if (cloner.hasClass('horizontally')) $('.qfrem a', rows)[0].style.opacity = 0.1;
    }


    function sumBox(form) {
      var priceBox = $('.qfpriceinner', form);
      form.suml = form.qfcod ? form.qfcod.value : schowRelated($(form), '');
      if (!priceBox.length) {
        $(form).trigger( "qfsetprice" );
        return;
      };

      if (form.formul && form.formul.value) {
        qfCalculatorByFormul(form);
      } else {

        $('.calcCondition').each(function() {
          var d = $(this).data('settings');
          if (d.condition) {
            d.istrue = '';
            var boxsum = qfCalculator($("input, select", this));
            if (boxsum != 'error') {
              d.istrue = eval(d.condition.replace("s", boxsum.toFixed(10)));
            }
          }
        });

        var price = qfCalculator(form.elements);
        if (price == 'error') return;
        if (parseInt(price) != (price * 1)) {
          price = (Math.round(price * 100) / 100).toFixed(2);
        }
        priceBox.html(strPrice(price));
        $('input[name="qfprice[]"]', form).val(price);
        $(form).trigger( "qfsetprice" );


        if ($('.qfclonesum span', form).length) {
          $('.qfclonerrow').each(function() {
            var cprice = qfCalculator($("input, select", this));
            if (cprice == 'error') return;
            // if (parseInt(cprice) != (cprice * 1)) {
            //   cprice = (Math.round(cprice * 100) / 100).toFixed(2);
            // }
            $('.qfclonesum span', this).html(strPrice(cprice));
          });
        }
      }
    }


    function strPrice(x) {
      return $.QuickForm.strPrice(x);
    }

    function qfCalculator(filds) {
      var pat = '',
        sum = 0;
      for (var n = 0; n < filds.length; n++) {
        var add = getAdd(filds[n]);
        if (add) {
          pat += add;
        }
      }
      if (!pat) pat = 0;
      else pat = chekPat(pat);

      try {
        sum = eval(pat);
        if (isNaN(sum)) {
          throw new Error();
        }
      } catch (err) {
        $('.qfpriceinner', filds[0].form).html('ERROR: ' + pat);
        sum = 'error';
      }
      // console.log(pat);
      return sum;
    }


    function qfCalculatorByFormul(form) {
      var priceBox = $('.qfpriceinner', form);
      var pats = form.formul.value.split(/\s*;\s*/);
      for (var i = 0; i < pats.length; i++) {
        var pat = pats[i].split('=');
        if (pat.length == 2) {
          var n = pat[0].slice(3);
          var sumstr = pat[1];
          var price = getSumByFormul(form, sumstr, n);
          if (price == 'error') return;
          if (parseInt(price) != (price * 1)) {
            price = (Math.round(price * 100) / 100).toFixed(2);
            // price = parseFloat(price).toFixed(2);
          }
          if (priceBox[n]) {
            priceBox[n].innerHTML = strPrice(price);
            $('input[name="qfprice[]"]', form)[n].value = price;
          }
        }
      }
    }

    function getSumByFormul(form, sumstr, num) {
      var pat = '';
      var arr = sumstr.split('|');
      var els = form.elements;
      for (var n = 0; n < arr.length; n++) {
        for (var i = 0; i < els.length; i++) {
          var d = $(els[i]).data('settings');
          if (d && d.fildid) {
            if (d.fildid == arr[n]) {
              pat += getAdd(els[i]);
            }
          }
        }
      }

      if (!pat) pat = 0;
      else pat = chekPat(pat);

      try {
        sum = eval(pat);
        if (isNaN(sum)) {
          throw new Error();
        }
      } catch (err) {
        if ($('.qfpriceinner', form)[num]) {
          $('.qfpriceinner', form)[num].innerHTML = ('ERROR: ' + pat);
        } else $('.qfpriceinner', form).html('not price box num ' + num);
        sum = 'error';
      }

      return sum;
    }


    function getAdd(fild) {
      var add = '',
        d;

      if (fild.type === 'select-one') {
        d = $(fild.options[fild.selectedIndex]).data('settings');
      } else if (fild.type === 'radio' || fild.type === 'checkbox') {
        if (fild.checked) d = $(fild).data('settings');
      } else {
        d = $(fild).data('settings');
      }

      if (d && d.calculator) {
        if (d.cond) {
          var boxd = $(fild).parent().data('settings');
          if (!boxd.condition || !boxd.istrue) return '';
        }

        add = d.calculator.replace("v", (fild.value ? fild.value : 0));
      }

      return add;
    }


    function chekPat(pat) {
      return pat.replace(/,/g, ".").replace(/[^0-9()-.+\*\/]/g, '');
    }

  },

strPrice: function(x) {
  if (parseInt(x) != (x * 1)) {
    x = (Math.round(x * 100) / 100).toFixed(2);
  }

  x = x.toString();
  var y = x.charAt(0);
  for (var n = 1; n < x.length; n++) {
    if (Math.ceil((x.length - n) / 3) == (x.length - n) / 3) y = y + " ";
    y = y + x.charAt(n);
  }
  return y.replace(" .", ",");
},

prepareFormForSend: function (form) {
  $('input[name="qfradio[]"]', form).each(function() {
    var radios = $('input[type="radio"]', $(this).parent());
    for (var i = 0; i < radios.length; i++) {
      if (radios[i].checked) {
        this.value = radios[i].value;
      }
    }
  });

  $('input[name="qfcheckbox[]"]', form).each(function() {
    var chbx = $('input[type="checkbox"]', $(this).parent())[0];
    this.value = chbx.checked? 1 : 0;
  });

  $('input[name="qfcloner[]"]', form).each(function() {
    this.value = $(this).parent().children('.qfclonerrow').length;
  });

  if (!form.qftoken) {
    $(form).append('<input type="hidden" name="qftoken" value="' + form.sumb + '">');
  }

},

verticallycentr: function(box) {
  var winh = $(window).height();
  var scr = $(document).scrollTop();
  if (box.height() < winh) {
    var h = (winh - box.height()) / 2 + scr;
    box.animate({
      'opacity': 1,
      'top': h
    }, 600);
  } else {
    box.animate({
      'opacity': 1,
      'top': 100 + scr
    }, 600);
  }
},


dokeepalive: function() {
    var qfkeepalive = false,
      fu = 1;
    $('.qfkeepalive').each(function() {
      if (this.value > 0) {
        qfkeepalive = this.value;
      };
    });
    if (qfkeepalive) {
      $('script').each(function() {
        if (this.src.indexOf('keepalive.js') > 0) {
          fu = false;
          return false;
        }
      });
      if (fu) window.setInterval(function() {
        jQuery.ajax({
          data: {
            option: "com_quickform",
            task: "ajax",
            mod: "related",
            id: 0
          },
          success: function() {}
        });
      }, qfkeepalive)
    }
  },



  qfstartform: function(id) {
    if ($('.qfoverlay').length) {
      $('.qfoverlay').remove();
    }

    var over = $('<div class="qfoverlay"></div>').appendTo(document.body);
    var qfoverop = over.css('opacity');
    var box = $('<div class="qfmodalform"><div class="qfclose">×</div></div>').appendTo(document.body);

    var boxclose = function() {
      over.add(box).stop().animate({
        'opacity': 0
      }, 600, function() {
        $(this).remove();
      });
    }

    $('.qfclose').click(function() {
      boxclose();
    })

    over.css({
      'display': 'block',
      'opacity': 0,
      'height': $(document).height()
    }).animate({
      'opacity': qfoverop
    }, 600).click(function() {
      boxclose();
    });

    box.css({
      'display': 'block',
      'opacity': 0
    });

    jQuery.ajax({
      url: '/index.php',
      data: {
        option: "com_quickform",
        task: "ajax",
        mod: "qfmodal",
        id: id
      },
      success: function(html) {
        box.append(html);
        $.QuickForm.verticallycentr(box);
        var form = $('form', box)[0];
        $.QuickForm.initiate(form);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        // console.log();
      }
    });

    return;
  }

}
})(jQuery);
