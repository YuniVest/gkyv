<?php
/**
* @package		Joomla & QuickForm
* @Copyright ((c) juice-lab.ru
* @license    GNU/GPL
*/


//  атрибуты, скобки, сепараторы, 


defined('_JEXEC') or die;

class QuickformViewForms extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	public function display($tpl = null)
	{
		$this->items         = $this->get('Items');
		$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');
		$this->projectTitle         = $this->get('projectTitle');

		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		require_once JPATH_COMPONENT.'/helpers/quickform.php';
		QuickformHelper::addSubmenu('projects');

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();

		return parent::display($tpl);
	}


	protected function addToolbar()
	{
		$canDo = JHelperContent::getActions('com_quickform');
		JToolbarHelper::title(JText::_('QF_LIST_FIELDS'), 'bookmark banners');

		if ($canDo->get('core.create'))
		{
			JToolbarHelper::addNew('form.add');
		}

		if ($canDo->get('core.delete'))
		{
			JToolbarHelper::divider();
			JToolbarHelper::deleteList('JGLOBAL_CONFIRM_DELETE', 'forms.delete');
		}

		if ($canDo->get('core.admin'))
		{
			JToolbarHelper::preferences('com_quickform');
		}

		JToolbarHelper::divider();
		JToolbarHelper::help('JHELP_COMPONENTS_MESSAGING_INBOX');
		
		JHtmlSidebar::setAction('index.php?option=com_quickform&view=forms');
	}

	protected function getSortFields()
	{
		return array(
			'ordering' => JText::_('JGRID_HEADING_ORDERING'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}
}

