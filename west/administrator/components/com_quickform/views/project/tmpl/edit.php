<?php
/**
* @package		Joomla & QuickForm
* @Copyright ((c) juice-lab.ru
* @license    GNU/GPL
*/

defined('_JEXEC') or die;

// Include the HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');

JFactory::getDocument()->addScriptDeclaration("
		Joomla.submitbutton = function(task)
		{
			if (task == 'project.cancel' || document.formvalidator.isValid(document.getElementById('project-form')))
			{
				Joomla.submitform(task, document.getElementById('project-form'));
			}
		};
");
JFactory::getDocument()->addStyleDeclaration("
#calculator textarea{
	width: auto;
}
");
?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		var modallink = function(){
			var box = $('#jform_params_modallink').closest('.control-group');
			if ($('#jform_params_modal').val()==1) {
				box.show();
			}
			else {
				box.hide();
			}
		}

		modallink();

		$('#jform_params_modal').change(function(){
			modallink();
		});

	});
</script>

<form action="<?php echo JRoute::_('index.php?option=com_quickform&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="project-form" class="form-validate form-horizontal">
	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('QF_FORM', true)); ?>
		<div class="row-fluid">
			<div class="span9">
				<?php
          echo $this->form->getControlGroup('title');
          echo $this->form->getControlGroup('cssstyle');
          echo $this->form->getControlGroup('popmess');
        ?>
			</div>
			<div class="span3">
				<?php echo JLayoutHelper::render('joomla.edit.global', $this); ?>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'message', JText::_('QF_LETTER', true)); ?>
			<?php echo $this->form->getControlGroups('message'); ?>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'calculator', JText::_('QF_CALCULATOR', true)); ?>
    <div style="padding:20px"><?php echo JText::_('QF_CALCULATOR_FORMUL_DESK') ?> <br /><br /></div>
			<?php echo $this->form->getControlGroups('calculator'); ?>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'params', JText::_('JGLOBAL_FIELDSET_ADVANCED', true)); ?>
			<?php echo $this->form->getControlGroups('params'); ?>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
	</div>

	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>


</form>
