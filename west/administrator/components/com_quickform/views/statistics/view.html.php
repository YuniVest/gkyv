<?php
/**
* @package		Joomla & QuickForm
* @Copyright ((c) juice-lab.ru
* @license    GNU/GPL
*/

defined('_JEXEC') or die;

class QuickformViewStatistics extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		require_once JPATH_COMPONENT.'/helpers/quickform.php';
		
		QuickformHelper::addSubmenu('statistics');
		
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		
		parent::display($tpl);
	}

	protected function addToolbar()
	{
		$canDo = JHelperContent::getActions('com_quickform');
		JToolbarHelper::title(JText::_('QF_HISTORY_LIST'), 'bookmark banners');
		
		if ($canDo->get('core.edit.state')) {
			JToolBarHelper::divider();
			JToolBarHelper::checkin('statistics.checkin');
		}
		
		if ($canDo->get('core.delete'))
		{
			JToolbarHelper::divider();
			JToolbarHelper::deleteList('JGLOBAL_CONFIRM_DELETE', 'statistics.delete');
		}
		
		if ($canDo->get('core.admin'))
		{
			JToolbarHelper::preferences('com_quickform');
		}

		JToolbarHelper::divider();
		JToolbarHelper::help('JHELP_COMPONENTS_MESSAGING_INBOX');
		
		JHtmlSidebar::setAction('index.php?option=com_quickform&view=statistics');
		
		JHtmlSidebar::addFilter(
			JText::_('QF_SELECT_STATUS'),
			'filter_st_status',
			JHtml::_('select.options', QuickFormHelper::getStatus(), 'value', 'text', $this->state->get('filter.st_status'), true)
		);
	}
	
}
