CREATE TABLE IF NOT EXISTS `#__quickform_projects` (
  `id` int(11) NOT NULL auto_increment,
  `access` int(11) NOT NULL default '1',
  `published` tinyint(1) NOT NULL DEFAULT '1' ,
  `checked_out` int(11) NOT NULL default '0',
  `checked_out_time` datetime,
  `title` varchar(256) NOT NULL default '',
  `language` char(7) NOT NULL default '*',
  `cssstyle` varchar(50) NOT NULL default '',
  `ordering` int(11) NOT NULL default '0',
  `toemail` varchar(128) NOT NULL default '',
  `tmpl` varchar(50) NOT NULL default '',
  `calculator` text,
  `showtitle` tinyint(1) NOT NULL DEFAULT '0',
  `popmess` text,
  `subject` varchar(256) NOT NULL default '',
  `hits` int(11) NOT NULL default '0',
  `params` text,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__quickform_forms` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(256) NOT NULL default '',
  `checked_out` int(11) NOT NULL default '0',
  `checked_out_time` datetime,
  `ordering` int(11) NOT NULL default '0',
  `fields` text,
  `params` text,
  `projectid` int(11) NOT NULL default '0',
  `def` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__quickform_ps` (
  `id` int(11) NOT NULL auto_increment,
  `st_title` varchar(256) NOT NULL default '',
  `st_formid` int(11) NOT NULL default '0',
  `st_form` text,
  `st_date` varchar(50) NOT NULL default '',
  `st_status` tinyint(1) NOT NULL default '0',
  `st_ip` varchar(128) NOT NULL default '',
  `st_user` int(11) NOT NULL default '0',
  `checked_out` int(11) NOT NULL default '0',
  `checked_out_time` datetime,
  `st_desk` text,
  `params` text,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;
