<?php
/**
 * Latest SEO Glossary module Default layout
 *
 * We developed this code with our hearts and passion.
 * We hope you found it useful, easy to understand and change.
 * Otherwise, please feel free to contact us at contact@joomunited.com
 *
 * @package 	SEO Glossary
 * @copyright 	Copyright (C) 2012 JoomUnited (http://www.joomunited.com). All rights reserved.
 * @license 	GNU General Public License version 2 or later; http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined( '_JEXEC' ) or die ;
$d = JFactory::getDocument();
$d->addStyleSheet(JURI::base().'components/com_seoglossary/assets/css/style.css');
?>

<div class="seoglossary_module_<?php echo htmlspecialchars( $params->get( 'moduleclass_sfx' ) );?>">
	<?php echo modSearchseoglossaryHelper::getSearchform($params); ?>
</div>
