<?php
/**
* @package		Joomla & QuickForm
* @Copyright ((c) bigemot.ru
* @license    GNU/GPL
*/

defined('_JEXEC') or die;

if (!$params->get('mod_type')) {

    require_once("components/com_quickform/classes/buildform.php");
    $qf = new QuickForm();
    echo $qf->getQuickForm((int)$params->get('id'));
}
else {

    JHtml::_('jquery.ui');

    if ($params->get('cartcss') != 'none') {
      JHtml::_('stylesheet', 'modules/mod_quickform/css/' . $params->get('cartcss'), array('version' => 'auto'));
    }

    JHtml::_('script', 'components/com_quickform/assets/js/quickform.js', array('version' => 'auto'));
    JHtml::_('script', 'modules/mod_quickform/js/qf_cart.js', array('version' => 'auto'));

    $comqf_params = JComponentHelper::getParams('com_quickform');


    require_once("components/com_quickform/classes/qfcart.php");
    $qfcart = new qfCart();
    echo '<script>var QF_TEXT_2 = "'.JText::_($comqf_params->get('text_2')).'";</script><div class="qf_cart_box">'.$qfcart->getMiniCartHtml().'</div>';



}
