<?php
defined('_AKEEBA_RESTORATION') or die('Restricted access');
$restoration_setup = array(
	'kickstart.security.password' => 'pr5tK3Gl9KhYjcx6DuMdAlSpzrhW5xq5',
	'kickstart.tuning.max_exec_time' => '5',
	'kickstart.tuning.run_time_bias' => '75',
	'kickstart.tuning.min_exec_time' => '0',
	'kickstart.procengine' => 'direct',
	'kickstart.setup.sourcefile' => '/volume1/web/Unics/tmp/Joomla_3.9.4-Stable-Update_Package.zip',
	'kickstart.setup.destdir' => '/var/www/html/gkyv/unics',
	'kickstart.setup.restoreperms' => '0',
	'kickstart.setup.filetype' => 'zip',
	'kickstart.setup.dryrun' => '0',
	'kickstart.setup.renamefiles' => array(),
	'kickstart.setup.postrenamefiles' => false);