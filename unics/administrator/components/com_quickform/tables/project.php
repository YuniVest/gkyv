<?php
/**
* @package		Joomla & QuickForm
* @Copyright ((c) juice-lab.ru
* @license    GNU/GPL
*/
defined('_JEXEC') or die;

class QuickformTableProject extends JTable
{

	function __construct(& $db) {
		parent::__construct('#__quickform_projects', 'id', $db);
	}

	function bind($array, $ignore = '')
	{
		if (isset($array['params']) && is_array($array['params'])) {
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = (string)$registry;
		}

		return parent::bind($array, $ignore);
	}
	
	public function store($updateNulls = false)
	{
		if ($this->id) {
//			// Existing item
		} else {
			$this->ordering = $this->getNextOrder();
		}
		return parent::store($updateNulls);
	}

	public function check()
	{
		if (trim($this->title) == '') {
			$this->setError(JText::_('Error. Title not found'));
			return false;
		}

		return true;
	}

	
}
