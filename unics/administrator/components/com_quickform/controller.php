<?php
/**
* @package		Joomla & QuickForm
* @Copyright ((c) juice-lab.ru
* @license    GNU/GPL
*/

defined('_JEXEC') or die;

class QuickformController extends JControllerLegacy
{
	protected $default_view = 'projects';

	public function display($cachable = false, $urlparams = array())
	{
		require_once JPATH_COMPONENT . '/helpers/quickform.php';

		if($this->input->get('task', '')=='ajax'){
			require_once JPATH_COMPONENT.'/helpers/ajax.php';
			exit;
		}

		$view   = $this->input->get('view', 'projects');
		$layout = $this->input->get('layout', 'default');
		$id     = $this->input->getInt('id');

		if ($view == 'form' && $layout == 'edit' && !$this->checkEditId('com_quickform.edit.form', $id))
		{
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_quickform&view=projects', false));

			return false;
		}


		return parent::display();
	}

}
