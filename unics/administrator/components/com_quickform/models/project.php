<?php
/**
* @package		Joomla & QuickForm
* @Copyright ((c) juice-lab.ru
* @license    GNU/GPL
*/

defined('_JEXEC') or die;

class QuickformModelProject extends JModelAdmin
{
    public function getTable($type = 'Project', $prefix = 'QuickformTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getForm($data = array(), $loadData = true)
    {
        $form = $this->loadForm('com_quickform.project', 'project', array('control' => 'jform', 'load_data' => $loadData));

        if (empty($form)) {
            return false;
        }

        return $form;
    }

    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState('com_quickform.edit.project.data', array());

        if (empty($data)) {
            $data = $this->getItem();
        }

        $this->preprocessData('com_quickform.project', $data);

        return $data;
    }

    protected function prepareTable($table)
    {
        $table->title = htmlspecialchars_decode($table->title, ENT_QUOTES);
    }

    protected function canDelete($record)
    {
        if (parent::canDelete($record)) {
            $db = $this->getDbo();

            $db->setQuery(
                    'DELETE FROM '.$db->quoteName('#__quickform_forms') .
                    ' WHERE '.$db->quoteName('projectid').' = '.(int) $record->id
                );
            $db->query();

            if ($db->getErrorNum()) {
                $this->setError($db->getErrorMsg());
                return false;
            }

            return true;
        } else {
            return false;
        }
    }
}
