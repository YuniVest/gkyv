/**
 * @package		Joomla & QuickForm
 * @Copyright ((c) juice-lab.ru
 * @license    GNU/GPL
 */

jQuery(document).ready(function($) {

    var fixHelper;
    fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };

    $('#formtbl').find('tbody').sortable({
        helper: fixHelper
    });

    var tableRef = document.getElementById('formtbl').getElementsByTagName('tbody')[0];
    jQuery('tr', tableRef).each(function() {
        fildActivate(this);
    });


    $('#toolbar-iconaddfild').find('button').click(function() {
        var qfmenu = $('#qfmenu');
        qfmenu.length ? qfmenu.remove() : domenu();
        return false;
    });

    function domenu() {
        var els = ['select', 'textarea', 'submit'];
        var els2 = ['button', 'checkbox', 'file', 'hidden', 'radio', 'reset', 'text'];
        var els3 = ['color', 'date', 'email', 'number', 'range', 'tel', 'url'];
        var els4 = ['customHtml', 'customPhp', 'calculatorSum', 'recaptcha', 'cloner', 'calcCondition', 'backemail', 'addToCart', 'qftabs'];

        var rows = function(arr, cl) {
            var html = '';
            arr.forEach(function(el) {
                html += '<li><a href="#" class="' + cl + '">' + el + '</a></li>';
            });
            return html;
        };

        var html = '<ul id="qfmenu"><li><a href="#" class="noqfinp">input</a><ul>';
        html += rows(els2, 'qfinp');
        html += '</ul></li>';
        html += '<li><a href="#" class="noqfinp">HTML5</a><ul>';
        html += rows(els3, 'qfinp');
        html += '</ul></li>';
        html += ' <li><a href="#" class="noqfinp">QuickForm</a><ul>';
        html += rows(els4, 'qfqf');
        html += '</ul></li>';
        html += rows(els, 'qf0');
        html += '</ul>';

        $('#toolbar-iconaddfild').append(html);
        $('#qfmenu').mouseleave(function() {
            $(this).remove();
        }).find('a').click(function() {
            addfild(this);
            $('#qfmenu').remove();
            return false;
        });
    }

    function addfild(el) {
        var teg, leb;
        if (el.className == 'noqfinp') {
            return false;
        }

        if (el.className == 'qfinp') {
            teg = 'input[' + el.innerHTML + ']';
        } else teg = el.innerHTML;
        var rteg = teg;

        if (el.innerHTML == 'select' || el.innerHTML == 'radio' || el.innerHTML == 'qftabs') {
            teg = '<span class="smbtogl"></span> <a href="#" class="optionstogler">' + teg + '</a>';
        }

        var newRow = tableRef.insertRow(tableRef.rows.length);

        if (rteg == 'customHtml') {
            leb = '<textarea class="qflabelclass">your code here...</textarea>';
        } else if (rteg == 'recaptcha' || rteg == 'cloner' || rteg == 'calcCondition') {
            leb = '<input type="hidden" value="" class="qflabelclass" />';
        } else if (rteg == 'backemail') {
            leb = '<input name="qfllabel" type="text" value="Send a copy of this message to your own address" class="qflabelclass" />';
        } else {
            leb = '<input name="qfllabel" type="text" value="" class="qflabelclass" />';
        }

        newRow.innerHTML = '<td class="l_td">' + leb + '</td><td class="r_td">' + teg + '</td><td class="atr_td"><a href="#"><img src="components/com_quickform/assets/setting.png"></a></td><td class="del_td"><a href="#"><img src="components/com_quickform/assets/delete.png"></a></td>';
        $(newRow).data('settings', {
            "fildnum": newfildnum(),
            'teg': rteg
        });
        fildActivate(newRow);
    }

    function fildActivate(row) {
        var inner = $('.r_td', row).html();
        if ((inner.indexOf('select') + 1) || (inner.indexOf('radio') + 1) || (inner.indexOf('qftabs') + 1)) {
            if (!$('.optionsBox', row).length) {
                $('.l_td', row).append('<div class="optionsBox"></div>');
                optionRowHtml().appendTo($('.optionsBox', row));
            }
            if(inner.indexOf('qftabs') + 1){
              optionsBoxActivate(row, false);
            }
            else{
              optionsBoxActivate(row, true);
            }
        }

        $('.del_td a', row).click(function() {
            $(row).remove();
            return false;
        });
        $('.atr_td a', row).click(function() {
            if ($('.boxsetting').length) return $('.boxsetting').remove();
            boxRowSetting(row);
            return false;
        });
    }

    function optionsBoxActivate(row, calc) {
        $('.optionRow', row).each(function() {
            optionRowActivate(this, calc);
        });

        if ($('.optionsBox', row).hasClass('hid')) {
            $('.smbtogl', row).html('►');
        } else {
            $('.smbtogl', row).html('▼');
        }

        $('.optionstogler', row).click(function() {
            if ($('.optionsBox', row).hasClass('hid')) {
                $('.smbtogl', row).html('▼');
                $('.optionsBox', row).removeClass('hid');
            } else {
                $('.smbtogl', row).html('►');
                $('.optionsBox', row).addClass('hid');
            }
            return false;
        });
    }

    function optionRowActivate(optionRow, calc) {
        $('.plus', optionRow).click(function() {
            insertOption(optionRow, calc);
            return false;
        });
        $('.delete', optionRow).click(function() {
            var optionsBox = $(optionRow).parent();
            if ($('.optionRow', optionsBox).length > 1) $(optionRow).remove();
            return false;
        });
        $('.setting', optionRow).click(function() {
            boxOptionSetting(optionRow, calc);
            return false;
        });
        if ($('input', optionRow).hasClass('calc')) $(optionRow).closest('.l_td').find('.qflabelclass').addClass('calc');
        if ($('input', optionRow).hasClass('related')) $(optionRow).closest('.l_td').find('.qflabelclass').addClass('related');
    }

    function insertOption(optionRow, calc) {
        var option = optionRowHtml().insertAfter($(optionRow));
        optionRowActivate(option, calc);
    }


    function optionRowHtml() {
        return $('<div class="optionRow" data-settings="{}"><input name="qfoption" type="text" value="" /><a href="#" class="setting"><img src="components/com_quickform/assets/setting.png"></a><a href="#" class="plus"><img src="components/com_quickform/assets/plus.png"></a><a href="#" class="delete"><img src="components/com_quickform/assets/delete.png"></a></div>');
    }

    function boxOptionSetting(optionRow, calc) {
        var dat = $(optionRow).data('settings');
        var box = getBoxBtns().appendTo($('body'));
        if(calc){
          box.append('<div class="boxtitle">' + $('input', optionRow).val() + '</div><div class="boxinner"><div class="boxinnerleft"><div class="boxmenu activ">related-fields</div><div class="boxmenu">calculator</div></div><div class="boxinnerright"><div class="boxbody related-fields activ">' + getForRelated(dat) + '</div><div class="boxbody calculator">' + getForCalculator(dat) + '</div></div></div>');
        }
        else{
          box.append('<div class="boxtitle">' + $('input', optionRow).val() + '</div><div class="boxinner"><div class="boxinnerleft"><div class="boxmenu activ">related-fields</div></div><div class="boxinnerright"><div class="boxbody related-fields activ">' + getForRelated(dat) + '</div></div></div>');
        }
        activateBox(box, optionRow);
    }

    function boxRowSetting(row) {
        var dat = $(row).data('settings');
        var boxInner = getBoxInner(dat);
        var box = getBoxBtns().appendTo($('body'));
        box.append('<div class="boxtitle">' + htmlentities($('.qflabelclass', row).val()).substr(0, 40) + '</div><div class="boxinner" data-teg="' + dat.teg + '"><div class="boxinnerleft">' + boxInner[0] + '</div><div class="boxinnerright">' + boxInner[1] + '</div></div>');
        activateBox(box, row);
    }

    function htmlentities(s) {
        var div = document.createElement('div');
        var text = document.createTextNode(s);
        div.appendChild(text);
        return div.innerHTML;
    }

    function escapeHtml(text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };
        return text.replace(/[&<>"']/g, function(m) {
            return map[m];
        });
    }

    function getBoxInner(dat) {
        if (dat.teg == 'select') return getBoxSelect(dat);
        else if (dat.teg == 'input[radio]') return getBoxRadio(dat);
        else if (dat.teg == 'input[checkbox]') return getBoxCheckbox(dat);
        else if (dat.teg == 'textarea') return getBoxTextarea(dat);
        else if (dat.teg == 'input[file]') return getBoxFile(dat);
        else if (dat.teg == 'input[hidden]') return getBoxHidden(dat);
        else if (dat.teg == 'input[text]') return getBoxText(dat);
        else if (dat.teg == 'input[color]') return getBoxColor(dat);
        else if (dat.teg == 'input[date]') return getBoxDate(dat);
        else if (dat.teg == 'input[email]') return getBoxEmail(dat);
        else if (dat.teg == 'input[url]') return getBoxEmail(dat);
        else if (dat.teg == 'input[number]') return getBoxText(dat);
        else if (dat.teg == 'input[range]') return getBoxRange(dat);
        else if (dat.teg == 'input[button]') return getBoxButton(dat);
        else if (dat.teg == 'input[reset]') return getBoxButton(dat);
        else if (dat.teg == 'submit') return getBoxSubmit(dat);
        else if (dat.teg == 'customHtml') return getBoxCustomHtml(dat);
        else if (dat.teg == 'calculatorSum') return getBoxCalculatorSum(dat);
        else if (dat.teg == 'recaptcha') return getBoxRecaptcha(dat);
        else if (dat.teg == 'cloner') return getBoxCloner(dat);
        else if (dat.teg == 'backemail') return getBoxBackemail(dat);
        else if (dat.teg == 'customPhp') return getBoxCustomPhp(dat);
        else if (dat.teg == 'calcCondition') return getBoxCondition(dat);
        else if (dat.teg == 'addToCart') return getBoxAddToCart(dat);
        else if (dat.teg == 'qftabs') return getBoxQftabs(dat);

        else return getBoxDef(dat);
    }



    function activateBox(box, row) {
        $('.boxdelete', box).click(function() {
            box.remove();
            return false;
        });

        $('.boxsave', box).click(function() {
            boxsave(box, row);
            return false;
        });

        $('.boxmenu', box).click(function() {
            $('.boxmenu', box).add($('.boxbody', box)).removeClass('activ');
            $(this).addClass('activ');
            $('.' + $(this).html(), box).addClass('activ');
            return false;
        });

        $('input[name="reg"]', box).change(function() {
            if (this.checked) {
                if (this.value) {
                    $('.reg2', box).css('display', '');
                    $('.reg1', box).css('display', 'none');
                } else {
                    $('.reg1', box).css('display', '');
                    $('.reg2', box).css('display', 'none');
                }
            }
            return false;
        });

        $('.customheder a', box).each(function() {
            var aCase = this.innerHTML;
            var area = $('.customfield', box);
            $(this).click(function() {
                area.val(area.val() + ' '+aCase+'=""');
                return false;
            });
        });

        if ('draggable' in box) box.draggable();
    }



    function boxsave(box, row) {
        var dat = $(row).data('settings');
        $('input, textarea', box).each(function() {
            var name = this.name;
            if (this.type == 'checkbox') {
                if (this.checked) {
                    eval('dat.' + name + ' = 1');
                } else {
                    eval('dat.' + name + ' = 0');
                }
            } else if (this.type == 'radio') {
                if (this.checked) eval('dat.' + name + ' = this.value');
            } else {
                if (name) eval('dat.' + name + ' = this.value');
            }
        });
        box.remove();
    }

    function getForRelated(dat) {
        return '<div class="boxbodyinner"><div class="boxbodyinnerhelp">' + QuickForm.JText('DESCRELATED') + '</div><br>field group ID: <input name="related" type="text" value="' + (dat.related ? dat.related : '') + '" class="qfrelatedclass" /></div>';
    }

    function getForCalculator(dat) {
        return '<div class="boxbodyinner"><div class="boxbodyinnerhelp">' + QuickForm.JText('DESCMAT') + '</div>pat: <input name="calculator" type="text" value="' + (dat.calculator ? dat.calculator : '') + '" class="qfcalculatorclass" /></div>';
    }

    function getForCustom(dat) {
        var inner = '';
        var arr = ['autocomplete', 'dirname', 'list', 'maxlength', 'pattern', 'max', 'min', 'readonly', 'step', 'size', 'value', 'onclick'];
        arr.forEach(function(el) {
            inner += ' '+el.link("#")+',';
        });

        return '<div class="boxbodyinner"><div class="customheder">' +inner+ ' etc.</div><textarea name="custom" class="customfield">' + (dat.custom ? escapeHtml(dat.custom) : '') + '</textarea></div>';
    }


    function getForCalculatorCloner(dat) {
        return '<div class="boxbodyinner"><div class="boxbodyinnerhelp">' + QuickForm.JText('DESCMATCL') + '</div><div><span class="leb">start:</span><input name="clonerstart" type="text" value="' + (dat.clonerstart ? dat.clonerstart : '') + '" /></div><div><span class="leb">end:</span><input name="clonerend" type="text" value="' + (dat.clonerend ? dat.clonerend : '') + '" /></div>';
    }

    function getBoxBtns() {
        return $('<div class="boxsetting"><a href="#" class="boxdelete"><img src="components/com_quickform/assets/delete.png"></a><a href="#" class="boxsave"><img src="components/com_quickform/assets/save.png"></a></div>');
    }

    function getBoxSelect(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        boxBody = qfst() + qffildnum(dat) + qfrequired(dat) + qfclass(dat) + qfhide(dat) + qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxRadio(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        boxBody = qfst() + qffildnum(dat) + qfclass(dat) + qforient(dat) + qfhide(dat) + qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxQftabs(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div>';
        boxBody = qfst() + qffildnum(dat) + qfclass(dat) + qforient(dat) + qfhide(dat) + qfend();
        return [menu, boxBody];
    }


    function getBoxCheckbox(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">related-fields</div><div class="boxmenu">calculator</div><div class="boxmenu">custom</div>';
        boxBody = qfst() + qffildnum(dat) + qfclass(dat) + qfrequired(dat) + qfpos2(dat) + qfchecked(dat) + qfhide(dat) + qfhidech(dat) + qfend();
        boxBody += '<div class="boxbody related-fields">' + getForRelated(dat) + '</div><div class="boxbody calculator">' + getForCalculator(dat) + '</div>';
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxTextarea(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        boxBody = qfst() + qffildnum(dat) + qfrequired(dat) + qfclass(dat) + qfplaceholder(dat) + qfhide(dat) + qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxFile(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        boxBody = qfst() + qffildnum(dat) + qfrequired(dat) + qfclass(dat) + qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxHidden(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">calculator</div><div class="boxmenu">custom</div>';
        boxBody = qfst() + qffildnum(dat) + qfvalue(dat) + qfhide(dat) + qfend();
        boxBody += '<div class="boxbody calculator">' + getForCalculator(dat) + '</div>';
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxText(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">calculator</div><div class="boxmenu">custom</div>';
        boxBody = qfst() + qffildnum(dat) + qfrequired(dat) + qfclass(dat) + qfplaceholder(dat) + qfend();
        boxBody += '<div class="boxbody calculator">' + getForCalculator(dat) + '</div>';
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxColor(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        boxBody = qfst() + qffildnum(dat) + qfclass(dat) + qfvalue(dat) + qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxDate(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        boxBody = qfst() + qffildnum(dat) + qfrequired(dat) + qfclass(dat) + qfvalue(dat) + qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxEmail(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        boxBody = qfst() + qffildnum(dat) + qfrequired(dat) + qfclass(dat) + qfplaceholder(dat) + qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxRange(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">calculator</div><div class="boxmenu">custom</div>';
        boxBody = qfst() + qffildnum(dat) + qfclass(dat) + qfend();
        boxBody += '<div class="boxbody calculator">' + getForCalculator(dat) + '</div>';
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxCustomHtml(dat) {
        var menu = '<div class="boxmenu activ">params</div>';
        var boxBody = qfst() + qffildnum(dat) + qfhide(dat) + qfhideForm(dat) + qfend();
        return [menu, boxBody];
    }

    function getBoxCustomPhp(dat) {
        var menu = '<div class="boxmenu activ">params</div><div class="boxmenu">form</div><div class="boxmenu">letter</div>';
        var boxBody = qfst() + qffildnum(dat) + qfend();
        boxBody += '<div class="boxbody form">' + qfcustomphp1(dat) + '</div>';
        boxBody += '<div class="boxbody letter">' + qfcustomphp2(dat) + '</div>';
        return [menu, boxBody];
    }


    function getBoxCondition(dat) {
      var menu, boxBody;
      menu = '<div class="boxmenu activ">params</div><div class="boxmenu">related-fields</div><div class="boxmenu">calculator</div>';
      boxBody = qfst() + qffildnum(dat) + '<div class="boxbodyinnerhelp"><br>' + QuickForm.JText('QF_CONDITION') + '</div>' + qfcondition(dat) + qfend();
      boxBody += '<div class="boxbody related-fields">' + getForRelated(dat) + '</div><div class="boxbody calculator">' + getForCalculatorCloner(dat) + '</div>';
      return [menu, boxBody];
    }


    function getBoxCalculatorSum(dat) {
        var menu = '<div class="boxmenu activ">params</div>';
        var boxBody = qfst() + qffildnum(dat) + qfunit(dat) + qfpos(dat) + '<div class="boxbodyinnerhelp">' + QuickForm.JText('CALCSUM') + '</div>' + qfend();
        return [menu, boxBody];
    }

    function getBoxRecaptcha(dat) {
        var menu = '<div class="boxmenu activ">params</div>';
        var boxBody = qfst() + qffildnum(dat) + qfcaptcha(dat) + '<div class="boxbodyinnerhelp">' + QuickForm.JText('DESCRECAPTCHA') + '</div>' + qfend();
        return [menu, boxBody];
    }

    function getBoxCloner(dat) {
        var menu, boxBody;
        menu = '<div class="boxmenu activ">params</div><div class="boxmenu">related-fields</div><div class="boxmenu">calculator</div>';
        boxBody = qfst() + qffildnum(dat) + qforient(dat) + qfsum(dat) + '<div class="boxbodyinnerhelp"><br>' + QuickForm.JText('MAXCLONS') + '</div>' + qfmax(dat) + qfend();
        boxBody += '<div class="boxbody related-fields">' + getForRelated(dat) + '</div><div class="boxbody calculator">' + getForCalculatorCloner(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxBackemail(dat) {
        var menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        var boxBody = qfst() + qffildnum(dat) + qfreg(dat);
        if (dat.reg) boxBody += '<div class="boxbodyinnerhelp reg1" style="display:none"><br>' + QuickForm.JText('QF_BACKEMAIL1') + '</div>' + '<div class="boxbodyinnerhelp reg2" style="display:"><br>' + QuickForm.JText('QF_BACKEMAIL2') + '</div>';
        else boxBody += '<div class="boxbodyinnerhelp reg1" style="display:"><br>' + QuickForm.JText('QF_BACKEMAIL1') + '</div>' + '<div class="boxbodyinnerhelp reg2" style="display:none"><br>' + QuickForm.JText('QF_BACKEMAIL2') + '</div>';
        boxBody += qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxButton(dat) {
        var menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        var boxBody = qfst() + qffildnum(dat) + qfbtnclass(dat) + qfvalue(dat) + qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxSubmit(dat) {
        var menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        var boxBody = qfst() + qffildnum(dat) + qfbtnclass(dat) + qfsubmitvalue(dat) + qfredirect(dat) + qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }


    function getBoxDef(dat) {
        var menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        var boxBody = qfst() + qffildnum(dat) + qfclass(dat) + qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }

    function getBoxAddToCart(dat) {
        var menu = '<div class="boxmenu activ">params</div><div class="boxmenu">custom</div>';
        var boxBody = qfst() + qffildnum(dat) + qfclass(dat) + qfcartvalue(dat) + '<div class="boxbodyinnerhelp"><br>Use this with module: mod_quickform</div>' + qfend();
        boxBody += '<div class="boxbody custom">' + getForCustom(dat) + '</div>';
        return [menu, boxBody];
    }


    function qfst() {
        return '<div class="boxbody params activ"><div class="boxbodyinner">';
    }

    function qfend() {
        return '</div></div>';
    }

    function qffildnum(dat) {
        return '<div><span class="leb">fieldid:</span><input type="text" name="fildnum" readonly value="' + dat.fildnum + '" /></div>';
    }

    function qfclass(dat) {
        return '<div><span class="leb">class:</span><input type="text" name="class" value="' + (dat.class ? dat.class : '') + '" /></div>';
    }

    function qfredirect(dat) {
      return '<div><span class="leb">redirect:</span><input type="text" name="redirect" value="' + (dat.redirect ? dat.redirect : '') + '" /></div>';
    }

    function qfbtnclass(dat) {
        var class1 = '';
        if (typeof(dat.class) == 'undefined') class1 = 'btn btn-primary';
        return '<div><span class="leb">class:</span><input type="text" name="class" value="' + (dat.class ? dat.class : class1) + '" /></div>';
    }

    function qfplaceholder(dat) {
        return '<div><span class="leb">placeholder:</span><input type="text" name="placeholder" value="' + (dat.placeholder ? escapeHtml(dat.placeholder) : '') + '" /></div>';
    }

    // function qfcustom(dat) {
    //     return '<div><span class="leb">' + "custom:".link("#") + '</span><input type="text" name="custom" value="' + (dat.custom ? escapeHtml(dat.custom) : '') + '" /></div>';
    // }

    function qfcustomphp1(dat) {
        return '<div><textarea name="customphp1" class="customphp">' + (('customphp1' in dat) ? escapeHtml(dat.customphp1) : '<div>example:</div>\r\n<?php echo "Hello world!"; ?>') + '</textarea></div>';
    }

    function qfcustomphp2(dat) {
        return '<div><textarea name="customphp2" class="customphp">' + (('customphp2' in dat) ? escapeHtml(dat.customphp2) : '<div>example:</div>\r\n<?php echo "Hello World"; ?>') + '</textarea></div>';
    }

    function qfrequired(dat) {
        return '<div><span class="leb">required:</span><input type="checkbox" name="required" value="1"' + (dat.required ? ' checked' : '') + ' /></div>';
    }

    function qfhide(dat) {
        return '<div><span class="leb2">' + QuickForm.JText('HIDELETTER') + ':</span><input type="checkbox" name="hide" value="1"' + (dat.hide ? ' checked' : '') + ' /></div>';
    }

    function qfhidech(dat) {
        return '<div><span class="leb2">' + QuickForm.JText('HIDEUNCHECKED') + ':</span><input type="checkbox" name="hidech" value="1"' + (dat.hidech ? ' checked' : '') + ' /></div>';
    }

    function qfsum(dat) {
        return '<div><span class="leb2">' + QuickForm.JText('SUMCLONER') + ':</span><input type="checkbox" name="sum" value="1"' + (dat.sum ? ' checked' : '') + ' /></div>';
    }

    function qfhideForm(dat) {
        return '<div><span class="leb2">' + QuickForm.JText('HIDEFORM') + ':</span><input type="checkbox" name="hideform" value="1"' + (dat.hideform ? ' checked' : '') + ' /></div>';
    }

    function qfchecked(dat) {
        return '<div><span class="leb">checked:</span><input type="checkbox" name="checked" value="1"' + (dat.checked ? ' checked' : '') + ' /></div>';
    }

    function qforient(dat) {
        return '<div><br>horizontally:<input type="radio" name="orient" value=""' + (!dat.orient ? ' checked' : '') + ' /> vertically:<input type="radio" name="orient" value="1"' + (dat.orient ? ' checked' : '') + ' /></div>';
    }

    function qfreg(dat) {
        return '<div><br>for all:<input type="radio" name="reg" value=""' + (!dat.reg ? ' checked' : '') + ' /> logged:<input type="radio" name="reg" value="1"' + (dat.reg ? ' checked' : '') + ' /></div>';
    }

    function qfcaptcha(dat) {
        return '<div>show all:<input type="radio" name="show" value=""' + (!dat.show ? ' checked' : '') + ' /> only guest:<input type="radio" name="show" value="1"' + (dat.show ? ' checked' : '') + ' /><br><br></div>';
    }

    function qfcondition(dat) {
      return '<div><span class="leb">condition:</span><input type="text" name="condition" value="' + (dat.condition ? escapeHtml(dat.condition) : '') + '" /></div>';
    }


    function qfunit(dat) {
        return '<div><span class="leb">unit:</span><input type="text" name="unit" value="' + (dat.unit ? escapeHtml(dat.unit) : '') + '" /></div>';
    }

    function qfpos(dat) {
        return '<div><br><span class="leb"></span>unit before:<input type="radio" name="pos" value=""' + (!dat.pos ? ' checked' : '') + ' /> unit after:<input type="radio" name="pos" value="1"' + (dat.pos ? ' checked' : '') + ' /></div>';
    }

    function qfpos2(dat) {
        return '<div><br><span class="leb">label:</span>before <input type="radio" name="pos" value=""' + (!dat.pos ? ' checked' : '') + ' /> after <input type="radio" name="pos" value="1"' + (dat.pos ? ' checked' : '') + ' /></div>';
    }

    function qfvalue(dat) {
        return '<div><span class="leb">value:</span><input type="text" name="value" value="' + (dat.value ? escapeHtml(dat.value) : '') + '" /></div>';
    }

    function qfsubmitvalue(dat) {
        return '<div><span class="leb">value:</span><input type="text" name="value" value="' + (('value' in dat) ? escapeHtml(dat.value) : 'QF_SUBMIT') + '" /></div>';
    }

    function qfcartvalue(dat) {
        return '<div><span class="leb">value:</span><input type="text" name="value" value="' + (('value' in dat) ? escapeHtml(dat.value) : 'QF_ADDTOCART') + '" /></div>';
    }


    function qfmax(dat) {
        return '<div><span class="leb">max:</span><input type="text" name="max" value="' + (dat.max ? dat.max : '') + '" /></div>';
    }


    function newfildnum() {
        var num = [];
        $('tr', tableRef).each(function() {
            var dat = $(this).data('settings');
            if (dat) num.push(dat.fildnum);
        });
        if (num.length) return (Math.max.apply(Math, num) + 1);
        else return 0;
    }

});




JSON.stringify = JSON.stringify || function(obj) {
    var t = typeof(obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string") obj = '"' + obj + '"';
        return String(obj);
    } else {
        // recurse array or object
        var n, v, json = [],
            arr = (obj && obj.constructor == Array);
        for (n in obj) {
            v = obj[n];
            t = typeof(v);
            if (t == "string") v = '"' + v + '"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};

var QuickForm = {
    /**
     * @return {string}
     */
    JText: function(str) {
        jQuery.ajax({
            url: "index.php?option=com_quickform&task=ajax&mod=jtext&str=" + str,
            success: function(msg) {
                var phrase = 'JText' + str;
                jQuery(":contains(" + phrase + ")").not(":has(:contains(" + phrase + "))").each(function() {
                    var that = jQuery(this);
                    var html = that.html();
                    html = html.replace(new RegExp(phrase, 'gi'), msg);
                    that.html(html);
                });
            }
        });
        return 'JText' + str;
    },

    submitform: function(task, form) {
        var field = [];
        var tableRef = document.getElementById('formtbl').getElementsByTagName('tbody')[0];
        jQuery('tr', tableRef).each(function(i, el) {
            var dat = jQuery(this).data('settings');
            dat.label = jQuery('.qflabelclass', this).val();

            if (dat.teg == 'select' || dat.teg == 'input[radio]' || dat.teg == 'qftabs') {
                var opt = [];
                jQuery('.optionRow', this).each(function(ii, el) {
                    var tmp = jQuery(this).data('settings');
                    tmp.label = jQuery('input[name="qfoption"]', this).val();
                    opt[ii] = tmp;
                });
                dat.options = opt;
            }

            field[i] = dat;
        });
        document.forms.adminForm.fields.value = JSON.stringify(field);
        Joomla.submitform(task, form);
    }
};
