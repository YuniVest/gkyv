<?php
/**    
 * SEO Glossary Catglossary view Edit layout
 *
 * We developed this code with our hearts and passion.
 * We hope you found it useful, easy to understand and change.
 * Otherwise, please feel free to contact us at contact@joomunited.com
 *
 * @package 	SEO Glossary
 * @copyright 	Copyright (C) 2012 JoomUnited (http://www.joomunited.com). All rights reserved.
 * @license 	GNU General Public License version 2 or later; http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'catglossary.cancel' || document.formvalidator.isValid(document.id('catglossary-form'))) {
			Joomla.submitform(task, document.getElementById('catglossary-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>
<style type="text/css">
	.seoglossary label
	{
		max-width:150px;
	}
</style>
<form action="<?php echo JRoute::_('index.php?option=com_seoglossary&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="catglossary-form" class="form-validate">
	<div>
		
           <div class="row-fluid">
			<div class="span9">
				<div class="row-fluid form-horizontal-desktop form-horizontal">
					<div class="span9">
		
			<?php /*echo $this->form->getLabel('id'); ?>
			<?php echo $this->form->renderField('id');*/ ?>
			<?php echo  $this->form->renderField('name'); ?>
			<?php echo $this->form->renderField('alias'); ?>
			<?php echo $this->form->renderField('icon'); ?>
			<?php echo $this->form->renderField('description'); ?>
			<?php echo $this->form->renderField('language'); ?>
			<?php echo $this->form->renderField('alphabet'); ?>
			<?php echo $this->form->renderField('state'); ?>
			<?php /* Hidden fields */ ?>
			<?php echo $this->form->renderField('checked_out'); ?>
            <?php echo $this->form->renderField('checked_out_time'); ?>
			</div>
			</div>
			</div>
		   </div>

	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
	<div class="clr"></div>
</form>