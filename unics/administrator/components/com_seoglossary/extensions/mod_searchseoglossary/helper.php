<?php
/**
 * Search Module SEO Glossary module helper
 *
 * We developed this code with our hearts and passion.
 * We hope you found it useful, easy to understand and change.
 * Otherwise, please feel free to contact us at contact@joomunited.com
 *
 * @package 	SEO Glossary
 * @copyright 	Copyright (C) 2014 JoomUnited (http://www.joomunited.com). All rights reserved.
 * @license 	GNU General Public License version 2 or later; http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die;
require_once(JPATH_SITE . '/components/com_seoglossary/helpers/compat.php');

SeogModel::addIncludePath(JPATH_SITE . '/components/com_seoglossary/models', 'SeoglossaryModel');

// import the Joomla! arrayhelper library
jimport('joomla.utilities.arrayhelper');

class modSearchseoglossaryHelper
{
    public static function getSearchform($params)
    {
        if ( (JFactory::getApplication()->input->getString( 'filter_search' ) == "") || (JFactory::getApplication()->input->getString( 'filter_search' ) == null) )
        {
            $value = strtolower( JText::_( 'COM_SEOGLOSSARY_SEARCH' ) ) . '...';
        }
        else
        {
            $value = JFactory::getApplication()->input->getString( 'filter_search' );
        }
        $filter_options = JFactory::getApplication()->input->getString( 'glossarysearchmethod', 1 );

        $str1 = "";
        $str2 = "";
        $str3 = "";
        $str4 = "";
        if ( $filter_options == 1 )
        {
            $str1 = 'checked="checked" ';
        }
        else
            if ( $filter_options == 2 )
            {
                $str2 = 'checked="checked" ';
            }
            else
                if ( $filter_options == 3 )
                {
                    $str3 = 'checked="checked" ';
                }
                else
                    if ( $filter_options == 4 )
                    {
                        $str4 = 'checked="checked" ';
                    }
        $Itemid = JFactory::getApplication()->input->getInt( "Itemid", '' );
        $catid = JFactory::getApplication()->input->getInt( "catid", '' );
        $url = JRoute::_( 'index.php?option=com_seoglossary&view=glossaries&catid=' . $params->get('catid'));
        $str = ' <div id="glossarysearchmodule"><form id="searchFormModule" name="searchForm" method="post" action="' . $url . '">
			<div id="glossarysearchmoduleheading">' . JText::_( "COM_SEOGLOSSARY_SEARCH_TEXT" ) . '</div>
   				<div class="input-append"><input type="text" title="'.strtolower( JText::_( 'COM_SEOGLOSSARY_SEARCH' ) ).'" id="filter_search_module" name="filter_search"  value="' . $value . '" size="25"
   					onblur="if(this.value==&quot;&quot;) this.value=&quot;' . strtolower( JText::_( 'COM_SEOGLOSSARY_SEARCH' ) ) . '...&quot;" onfocus="if(this.value==&quot;' . strtolower( JText::_( 'COM_SEOGLOSSARY_SEARCH' ) ) . '...&quot;) this.value=&quot;&quot;"
   				/>
                 <div class="dbl-btns clearfix">
                 <div class="sngl-btn srchbtn">
                 <button type="submit" class="button btn btn-primary">' . JText::_( "COM_SEOGLOSSARY_SEARCH" ) . '</button>
				</div><div class="sngl-btn clearbtn">
                <button onclick="document.id(&quot;filter_search_module&quot;).value=&quot;&quot;;this.form.submit();" type="submit" class="button btn clear-btn">' . JText::_( 'JSEARCH_FILTER_CLEAR' ) . '</button>
                </div></div>
				</div>';

        if ( $params->get( 'search_options', 1 ) != 0 )	{
            $default = $params->get('default_search_mode', 1);
            $str .=	'<div id="glossarysearchmethod" class="custom-select">
            	<select name="glossarysearchmethod" class="seoselect">
						<option ' . $str1 . ' value="1" '. ( ($default == 1) ? ' selected' : '' ) .' />' . JText::_( "COM_SEOGLOSSARY_SEARCH_BEGIN" ) . '
						<option ' . $str2 . ' value="2" '. ( ($default == 2) ? ' selected' : '' ) .' />' . JText::_( "COM_SEOGLOSSARY_SEARCH_CONTAINS" ) . '
						<option ' . $str3 . ' value="3" '. ( ($default == 3) ? ' selected' : '' ) .' />' . JText::_( "COM_SEOGLOSSARY_SEARCH_EXACT" );
            if ( $params->get( 'sound_like', 1 ) != 0 )
            {
                $str .= '<option ' . $str4 . ' value="4" '. ( ($default == 4) ? ' selected' : '' ) .' />' . JText::_( "COM_SEOGLOSSARY_SEARCH_LIKE" );
            }
            $str .= '</select></div>';
        } //endif ( $params->get( 'search_options', 1 ) != 0 )
        else {
            $str .= '<input type="hidden" name="glossarysearchmethod" value="'.$params->get('default_search_mode', 1).'" />';
        } // endelse ( $params->get( 'search_options', 1 ) != 0 )

        $str .= '<div>
				</div>
			</form>
		</div>';
        return $str;
    }
}