<?php

if (!$item['meta']) {
    return;
}

// Meta
$meta = $this->el('div', [

    'class' => [
        'el-meta',
        'uk-text-{meta_style: meta|muted|primary}',
        'uk-{meta_style: h\d} uk-margin-remove',
    ],

], $item['meta']);

echo $meta($element->props);
