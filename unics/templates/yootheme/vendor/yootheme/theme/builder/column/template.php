<?php

$el = $this->el('div', [
    'id' => $element['id'],
    'class' => $element['class'],
]);

$element['show_title'] = $element['style'] || $element['image'];
$element['media_overlay'] = $element['image'] ? $element['media_overlay'] : false;
$element['container'] = $element['media_overlay'] || $element['vertical_align'];

// Width
$breakpoints = ['s', 'm', 'l', 'xl'];
$breakpoint = $element->parent['breakpoint'];

// Above Breakpoint
$width = $element['widths'][0] ?: 'expand';
$width = $width === 'fixed' ? $element->parent['fixed_width'] : $width;
$el->attr('class', [
    "uk-width-{$width}".($breakpoint ? "@{$breakpoint}" : ''),

    // Vertical alignment
    'uk-grid-item-match uk-flex-{vertical_align} {@!show_title}',

    // Text color
    'uk-{text_color} {@!style: primary|secondary}' => !$element['style'] || $element['image'],

    'uk-grid-item-match {@show_title}',
]);

// Intermediate Breakpoint
if (isset($element['widths'][1]) && $pos = array_search($breakpoint, $breakpoints)) {
    $breakpoint = $breakpoints[$pos - 1];
    $width = $element['widths'][1] ?: 'expand';
    $el->attr('class', ["uk-width-{$width}".($breakpoint ? "@{$breakpoint}" : '')]);
}

// Order
if (!isset($element->parent->children[$element->index + 1]) && $element->parent['order_last']) {
    $el->attr('class', ["uk-flex-first@{$breakpoint}"]);
}

// Visibility
$visible = $element->count() ? 4 : false;
$visibilities = ['xs', 's', 'm', 'l', 'xl'];

foreach ($element as $ele) {
    $visible = min(array_search($ele['visibility'], $visibilities), $visible);
}

if ($visible) {
    $element['visibility'] = $visibilities[$visible];
    $el->attr('class', ["uk-visible@{$visibilities[$visible]}"]);
}

// Column options

// Tile

$tile = $this->el('div', [
    'class' => [
        'uk-tile',

        // Padding
        'uk-padding-remove {@padding: none}',
        'uk-tile-{!padding: |none}',

        // Vertical alignment
        // Can't use `uk-flex` and `uk-width-1-1` instead of `uk-grid-item-match` because it doesn't work with grid divider (it removes the ::before)
        'uk-grid-item-match uk-flex-{vertical_align}',
    ],
]);

$tile_container = $this->el('div', [
    'class' => [
        'uk-tile-{style}',

        // Text color
        'uk-preserve-color {@preserve_color} {@style: primary|secondary}',
    ],
]);

// Image
if ($element['image']) {

    $tile->attr($this->bgImage($element['image'], [
        'width' => $element['image_width'],
        'height' => $element['image_height'],
        'size' => $element['image_size'],
        'position' => $element['image_position'],
        'visibility' => $element['image_visibility'],
        'blend_mode' => $element['media_blend_mode'],
        'background' => $element['media_background'],
        'effect' => $element['image_effect'],
        'parallax_bgx_start' => $element['image_parallax_bgx_start'],
        'parallax_bgy_start' => $element['image_parallax_bgy_start'],
        'parallax_bgx_end' => $element['image_parallax_bgx_end'],
        'parallax_bgy_end' => $element['image_parallax_bgy_end'],
        'parallax_breakpoint' => $element['image_parallax_breakpoint'],
    ]));

    $tile_container->attr('class', [
        'uk-grid-item-match',
        'uk-position-relative {@media_overlay}',
    ]);

    // Overlay
    $overlay = $this->el('div', [
        'class' => ['uk-position-cover'],
        'style' => ['background-color: {media_overlay};'],
    ]);
}

// Fix margin if container
$container = $this->el('div', [
    'class' => [
        'uk-panel',

        // Make sure overlay is always below content
        'uk-position-relative {@image} {@media_overlay}',
    ],
]);

?>

<?= $el($element->props, $element['attrs']) ?>

    <?php if ($element['show_title']) : ?>
    <?= $tile_container($element->props, !$element['image'] ? $tile->attrs : []) ?>
    <?php endif ?>

        <?php if ($element['image']) : ?>
        <?= $tile($element->props) ?>
        <?php endif ?>

            <?php if ($element['media_overlay']) : ?>
            <?= $overlay($element->props, '') ?>
            <?php endif ?>

            <?php if ($element['container']) : ?>
            <?= $container($element->props) ?>
            <?php endif ?>

                <?= $element ?>

            <?php if ($element['container']) : ?>
            </div>
            <?php endif ?>

        <?php if ($element['image']) : ?>
        </div>
        <?php endif ?>

    <?php if ($element['show_title']) : ?>
    </div>
    <?php endif ?>

</div>
