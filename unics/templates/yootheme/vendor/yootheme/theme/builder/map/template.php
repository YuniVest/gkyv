<?php

$el = $this->el('div', [
    'id' => $element['id'],
    'class' => $element['class'],
]);

// Titles
foreach ($element['markers'] as $marker) {
    $marker->set('content', $this->render('@builder/map/template-marker', compact('marker')));
}

// Width and Height
$element['width'] = trim($element['width'], 'px');
$element['height'] = trim($element['height'] ?: '300', 'px');

// Position context
$el->attr([

    'class' => ['uk-position-relative uk-position-z-index'],

    'uk-responsive' => ['width: {width}; height: {height}'],

    'style' => [
        'width: {width}px {@!width_breakpoint}',
        'height: {height}px {@!width}',
    ],

    'uk-map' => json_encode(['map' => $element->all()]),

]);

?>

<?= $el($element->props, $element['attrs'], '') ?>
