<?php

if (!$item['meta']) {
    return;
}

// Meta
$el = $this->el('div', [
    'class' => [
        'el-meta',
        'uk-text-{meta_style: meta|muted|primary}',
        'uk-{meta_style: h\d} uk-margin-remove',
    ],
]);

echo $el($element->props, $item['meta']);
