<?php

$el = $this->el('div', [
    'id' => $element['id'],
    'class' => $element['class'],
]);

$el->attr('class', [
    'uk-panel {@!style}',
    'uk-card uk-card-body uk-{style}',
    'tm-child-list [tm-child-list-{list_style}] [uk-link-{link_style}] {@is_list}',
]);

// Title
$title = $this->el($element['title_tag'] ?: 'h3', [

    'class' => [
        'el-title',
        'uk-{title_style}',
        'uk-heading-{title_decoration}',
        'uk-card-title {@style} {@!title_style}',
        'uk-text-{!title_color: |background}',
    ],

]);

?>

<?= $el($element->props, $element['attrs']) ?>

    <?php if ($element['showtitle']) : ?>
        <?= $title($element->props) ?>
        <?php if ($element['title_color'] == 'background') : ?>
            <span class="uk-text-background"><?= $element->title ?></span>
        <?php elseif ($element['title_decoration'] == 'line') : ?>
            <span><?= $element->title ?></span>
        <?php else: ?>
            <?= $element->title ?>
        <?php endif ?>
        <?= $title->end() ?>

    <?php endif ?>

    <?= $element ?>

</div>
