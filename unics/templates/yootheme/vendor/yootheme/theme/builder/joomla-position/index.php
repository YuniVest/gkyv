<?php

$config = [

    'name' => 'yootheme/builder-joomla-position',

    'builder' => 'joomla_position',

    'inject' => [

        'view' => 'app.view',
        'scripts' => 'app.scripts',
        'document' => 'JFactory::getDocument',

    ],

    'render' => function ($element) {

        $renderer = $this->document->loadRenderer('modules');

        return $element['content'] && $this->document->countModules((string) $element['content'])
            ? $this->view->render('@builder/joomla-position/template', compact('element', 'renderer'))
            : '';
    },

    'events' => [

        'builder.init' => function ($elements, $builder) {
            $elements->set('joomla_position', json_decode(file_get_contents("{$this->path}/joomla-position.json"), true));
        },

    ],

    'config' => [

        'element' => true,
        'defaults' => [

            'layout' => 'stack',
            'breakpoint' => 'm',

        ],

    ],

];

return defined('_JEXEC') ? $config : false;
