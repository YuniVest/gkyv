<?php

$el = $this->el('div', [
    'id' => $element['id'],
    'class' => $element['class'],
]);

// Position
$name = (string) $element;
$style = $element['layout'] == 'stack' ? 'grid-stack' : 'grid';

echo $el($element->props, $element['attrs'], $renderer->render($name, compact('name', 'style')));
