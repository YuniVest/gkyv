<?php

return [

    'name' => 'yootheme/cookie',

    'inject' => [

        'view' => 'app.view',
        'scripts' => 'app.scripts',
        'request' => 'app.request',
        'customizer' => 'theme.customizer',

    ],

    'events' => [

        'theme.init' => function ($theme) {

            // set defaults
            $theme->merge($this->config['defaults'], true);
        },

        'theme.site' => function ($theme) {

            if (!$theme->get('cookie.active') || !$theme->get('cookie.message')) {
                return;
            }

            $this->view->addLoader(function ($name, $parameters, $next) {
                return ($name === 'header' ? $this->view->render('cookie') : '').$next($name, $parameters);
            });

            if ($this->customizer->isActive() || $this->request->getCookieParam('_cookieAllowed')) {
                return;
            }

            $this->scripts->add('cookie', "{$this->path}/app/cookie.min.js", 'theme-uikit', ['defer' => true]);

        },

        'theme.admin' => function ($theme) {

            $this->scripts->add('cookie-panel', "{$this->path}/app/cookie-panel.min.js", 'customizer');

        },

    ],

    'config' => [

        'panels' => [

            'cookie' => [

                'title' => 'Cookie Banner',
                'width' => 400,
                'fields' => [

                    'cookie.active' => [
                        'label' => 'Cookie Banner',
                        'description' => 'Show a banner to inform your visitors of cookies used by your website and to receive their consent.',
                        'text' => 'Enable Cookie Banner',
                        'type' => 'checkbox',
                    ],

                    'cookie.type' => [
                        'label' => 'Type',
                        'description' => 'Choose between an attached bar or a notification.',
                        'type' => 'select',
                        'options' => [
                            'Bar' => 'bar',
                            'Notification' => 'notification',
                        ],
                    ],

                    'cookie.bar_position' => [
                        'label' => 'Position',
                        'description' => 'The bar at the top pushes the content down while the bar at the bottom is fixed above the content.',
                        'type' => 'select',
                        'options' => [
                            'Top' => 'top',
                            'Bottom ' => 'bottom',
                        ],
                        'show' => 'cookie.type == "bar"',
                    ],

                    'cookie.bar_style' => [
                        'label' => 'Style',
                        'type' => 'select',
                        'options' => [
                            'Default' => 'default',
                            'Muted ' => 'muted',
                            'Primary' => 'primary',
                            'Secondary ' => 'secondary',
                        ],
                        'show' => 'cookie.type == "bar"',
                    ],

                    'cookie.notification_position' => [
                        'label' => 'Position',
                        'type' => 'select',
                        'options' => [
                            'Bottom Left' => 'bottom-left',
                            'Bottom Center' => 'bottom-center',
                            'Bottom Right' => 'bottom-right',
                        ],
                        'show' => 'cookie.type == "notification"',
                    ],

                    'cookie.notification_style' => [
                        'label' => 'Style',
                        'type' => 'select',
                        'default' => '',
                        'options' => [
                            'Default' => '',
                            'Primary' => 'primary',
                            'Warning ' => 'warning',
                            'Danger ' => 'danger',
                        ],
                        'show' => 'cookie.type == "notification"',
                    ],

                    'cookie.message' => [
                        'label' => 'Content',
                        'description' => 'Enter the cookie consent message. The default text serves as illustration. Please adjust it according to the cookie laws of your country.',
                        'type' => 'editor',
                        'editor' => 'visual',
                    ],

                    'cookie.button_style' => [
                        'label' => 'Button Style',
                        'type' => 'select',
                        'options' => [
                            'Close Icon' => 'icon',
                            'Button Default' => 'default',
                            'Button Primary' => 'primary',
                            'Button Secondary' => 'secondary',
                            'Button Text' => 'text',
                        ],
                    ],

                    'cookie.button_text' => [
                        'label' => 'Button Text',
                        'description' => 'Enter the text for the button.',
                        'enable' => 'cookie.button_style != "icon"',
                    ],

                ],

            ],

        ],

        'defaults' => [

            'cookie' => [

                'type' => 'bar',
                'bar_position' => 'bottom',
                'bar_style' => 'muted',
                'notification_position' => 'bottom-center',
                'message' => 'By using this website, you agree to the use of cookies as described in our Privacy Policy.',
                'button_style' => 'icon',
                'button_text' => 'Ok',

            ],

        ],

    ],

];
