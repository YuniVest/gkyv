<?php

$config = [

    'name' => 'yootheme/joomla-articles',

    'inject' => [

        'scripts' => 'app.scripts',

    ],

    'events' => [

        'content.data' => function ($context, $data) {

            if ($context != 'com_content.article' || !$data->id) {
                return;
            }

            \JLoader::register('ContentHelperRoute', JPATH_SITE.'/components/com_content/helpers/route.php');

            $route = \JRouter::getInstance('site')->build(\ContentHelperRoute::getArticleRoute($data->id, $data->catid));
            $installer = \JPluginHelper::getPlugin('installer', 'yootheme');

            $this->scripts
                ->add('$articles', "{$this->path}/app/articles.min.js", '$articles-data')
                ->add('$articles-data', sprintf('var $customizer = %s;', json_encode([
                    'context' => $context,
                    'apikey' => $installer ? (new \JRegistry($installer->params))->get('apikey') : false,
                    'url' => $this->app->url(($this->app['admin'] ? 'administrator/' : '').'index.php?p=customizer&option=com_ajax', [
                        'section' => 'builder',
                        'site' => $this->app['admin'] ? preg_replace('/\/administrator/', '', $route, 1) : (string) $route,
                    ]),
                ])), [], 'string');

        },

        'content.beforeSave' => function ($context, $article, $input) {

            if ($context != 'com_content.form' && $context != 'com_content.article') {
                return;
            }

            $form = $input->get('jform', null, 'RAW');

            // keep builder data, when JText filters are active
            if (isset($form['articletext']) && preg_match('/<!--\s{.*}\s-->\s*$/', $form['articletext'], $matches)) {
                $article->fulltext = $matches[0];
            }

        },

    ],

];

return defined('_JEXEC') ? $config : false;
