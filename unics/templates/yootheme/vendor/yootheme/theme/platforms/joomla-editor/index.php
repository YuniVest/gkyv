<?php

$config = [

    'name' => 'yootheme/joomla-editor',

    'inject' => [

        'scripts' => 'app.scripts',
        'conf' => 'JFactory::getConfig',
        'customizer' => 'theme.customizer',

    ],

    'events' => [

        'theme.admin' => function () {

            $this->scripts->add('customizer-editor', "{$this->path}/app/editor.min.js", 'customizer');

            // load editor
            $editor = $this->conf->get('editor');

            if (in_array($editor, ['tinymce', 'jce'])) {
                // all good, use enabled visual editor
            } elseif (JPluginHelper::getPlugin('editors', 'tinymce') && !in_array($editor, ['none', 'codemirror'])) {
                // tinymce installed? use as visual
                $editor = 'tinymce';
            } else {
                $editor = null;
            }

            if ($editor) {

                JHtml::_('behavior.modal');
                JHtml::_('jquery.framework');

                $instance = JEditor::getInstance($editor);

                if ($editor === 'jce') {
                    $this->customizer->mergeData([
                        'editorButtonsXtd' => JLayoutHelper::render(
                            'joomla.editors.buttons',
                            $instance->getButtons('yo_dummy_editor', ['pagebreak', 'readmore', 'widgetkit'])
                        ),
                    ]);
                }

                $instance->display('yo_dummy_editor', '', '', '', '', '', ['pagebreak', 'readmore', 'widgetkit']);

            }

        },

    ],

];

return defined('_JEXEC') ? $config : false;
