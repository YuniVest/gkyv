<?php
	/**
	 * Content SEO Glossary plugin file
	 *
	 * We developed this code with our hearts and passion.
	 * We hope you found it useful, easy to understand and change.
	 * Otherwise, please feel free to contact us at contact@joomunited.com
	 *
	 * @package 	SEO Glossary
	 * @copyright 	Copyright (C) 2012 JoomUnited (http://www.joomunited.com). All rights reserved.
	 * @license 	GNU General Public License version 2 or later; http://www.gnu.org/licenses/gpl-2.0.html
	 */
	
	// No direct access.
	defined('_JEXEC') or die;
	
	jimport('joomla.plugin.plugin');
	
	/**
	 * SEO Glossary plugin.
	 *
	 */
	 require_once(JPATH_SITE . '/components/com_seoglossary/helpers/compat.php');
	class plgContentSeoglossary extends JPlugin
	{
		/**
		 * Constructor
		 *
		 * @access	  protected
		 * @param	   object  $subject The object to observe
		 * @param	   array   $config  An array that holds the plugin configuration
		 * @since	   1.5
		 */
		public function __construct(& $subject, $config)
		{
			if( !is_dir( JPATH_SITE . '/components/com_seoglossary/models' ) )
			{
				return true;
			}
			parent::__construct($subject, $config);
	
		}
		
		public function onBeforeDisplay(&$article, &$params, $limitstart)
		{
	
		}
		
			
		/**
		 * @since	1.6
		 */
		public function onContentPrepare($context, &$content, &$params, $limitstart) 
		{
		if (JFactory::getApplication()->input->getInt('diagnose',0) == 1) {
		    echo "<b>SEOGlossary : Triggering content plugin....</b><br />";
		}
			
			$doc = jfactory::getdocument();
			if($doc->getType() != 'html') return true;
			// we don't run in modal pages or other incomplete pages
			//$nogo = array('component','raw');
			//if(in_array(JFactory::getApplication()->input->getString('tmpl'),$nogo)) return true;
			$regex = "#{seog_disable*(.*?)}(.*?){/seog_disable}#s";
			$regex_enable = "#{seog_enable*(.*?)}(.*?){/seog_enable}#s";	
			
			$app = JFactory::getApplication();
			$component=array();
			$com_params = jcomponenthelper::getparams( 'com_seoglossary' );
			$disable_component=$com_params->get('disable_component');
			$component=explode('.',$context);
			$disable_contextarr=$com_params->get('disable_context');
			$disable_context=explode(',',$disable_contextarr);
			$found=true;
			$disable_menu=$com_params->get('disable_menu');
			$regexfound=false;
			if ($app->isAdmin())
			{
				return;
			}
			if (preg_match_all($regex_enable, $content->text, $matches, PREG_PATTERN_ORDER) > 0) {
				$regexfound=true;
			}
			
			
			 if($disable_component)
				{
				if (in_array($component[0], $disable_component)) {
				$found=false;
					}
				}
				if($disable_context)
				{
				if (in_array($context, $disable_context)) {
							
						$found=false;
					}
				}
			if($component[0]=="com_content")
			{
				$disable_category=$com_params->get('disable_category');
				if($disable_category)
				{
				if (in_array($content->catid, $disable_category)) {
					$found=false;
				}
				}
			}
			
			if($disable_menu)
			{
				$fmenu = $app->getMenu()->getActive();
				if($fmenu)
				{
				if (in_array($fmenu->id, $disable_menu)) {
						$found=false;
				}
				}
				
			}
			if ( !is_dir( JPATH_SITE . '/components/com_seoglossary/models' ) )
			{
				return true;
			}
	
			if ( $com_params->get( 'show_glossaryplugin', 1 ) == 0 )
			{
				return true;
			}
			if($regexfound)
			{
				
			}
			else if(!($found))
			{
				return true;
			}
			
			require_once JPATH_SITE . '/components/com_seoglossary/models/glossaries.php';
			require_once JPATH_SITE . '/components/com_seoglossary/helpers/seoglossary.php';
				$this->loadLanguage();
			$doc = JFactory::getDocument();
			$com_params = JComponentHelper::getParams( 'com_seoglossary' );
	
		$custom_css = $com_params->get('custom_css', '') . "\n";
		if (!defined('SEOG_CUSTOM_CSS')) {
		    $doc->addStyleDeclaration($custom_css);
		    define('SEOG_CUSTOM_CSS', 1);
		}
	
			if($com_params->get('show_glossaryplugin',1)==0 || $com_params->get('disable_tooltips',0) == 1 || $com_params->get('disable_javascript',0) == 1 )
			{
				return;
			}
			
			$tooltip_layout=$com_params->get('tooltip_layout',1);
			if($tooltip_layout==0){
			
			if($com_params->get('advance_jquery',1)==1)
				{
					if (defined('SEOGJ3')) {
				JHtml::_('jquery.framework');
					}
					else
					{
						$doc->addScript( '//ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js' );
					}
				}
				
			
			$delay = $com_params->get('tooltip_delay',1000);
			$tool_width=intval($com_params->get('tooltip_width','200'));
			$datatheme=$com_params->get('datatheme','qtip-plain');
			$datatipeventout=$com_params->get('datatipeventout','mouseout');
				
			$showclose=$com_params->get('showclose',0);
			if($showclose)
			{
				$mjs=",button: 'Close'";
			}
			else
			{
				$mjs="";
			}
			if($datatipeventout=="load")
			{
				
				$djs=",
			hide: {
			event: 'unfocus'
			}";
			}
			else
			{
				$datatipdelay=$com_params->get('datatipdelay',500);
				$djs=",
				hide: {
				delay: '".$datatipdelay."'
				}";
			}
			$tooltip_position=$com_params->get('tooltip_position','top center');
			$my_position=$com_params->get('my_position','bottom center');
			$js="";
			if($com_params->get('advance_jquery_conflict',0)==1)
			{
				$js.="jQuery.noConflict();";
			}
			    if (!defined('SEOG_JS')) {
				$doc->addScript( JURI::root(true)."/components/com_seoglossary/assets/js/jquery.qtip.js" );
				$doc->addStyleSheet( JURI::base(true)."/components/com_seoglossary/assets/css/jquery.qtip.css" );
			$js.="jQuery(document).ready(function() {jQuery('.mytool a,.mytool abbr').qtip({
			style: { classes: '".$datatheme."' },
			position: {
				   my: '".$my_position."',
				   at:'".$tooltip_position."',
				   target: 'mouse' ,
				   viewport: jQuery(window),
					adjust: {
					    method: 'flipinvert'
					}
			},
			content: {      
			    text: function(event, api) {
		        return jQuery(this).attr('title');
			},
			title: function(event, api) {
		        return jQuery(this).text();
			}
			".$mjs."
			}".$djs."
			})});";
			
			$doc->addScriptDeclaration($js);
			
				$tooltip_style=".qtip{width:95%!important;max-width:".$tool_width."px;}";
				
				if($datatheme=="qtip-plain")
				{
					$font_size=intval($com_params->get('font_size','12'));
					$background_color = $com_params->get('background_color', '#EEEEEE');
					$barder_color = $com_params->get('barder_color', '#FFFFFF');
					$font_color = $com_params->get('font_color', '#444444');
					$head_color=$com_params->get('backgroundhead_color', '#444444');
					$tooltip_style.=".qtip-default .qtip-titlebar{
					background-color:".$head_color.";
					color:".$font_color.";
					border-color: ".$barder_color.";	
					}
					.qtip-content
					{
					background-color:".$background_color.";
					color:".$font_color.";
					border-color: ".$barder_color.";	
					}
					.qtip-default
					{
					background-color:".$head_color.";
					border:1px solid ".$barder_color.";
					color:".$font_color.";
					font-size:".$font_size."px;
					}
					
					";
	
				}
				$doc->addStyleDeclaration($tooltip_style);
			define('SEOG_JS', 1);
			
			}
		}
		else if($tooltip_layout==2)
		{
			JHtml::_('bootstrap.popover');
		}
		else
		{
			if($com_params->get('advance_jquery',1)==1)
			{
				if (defined('SEOGJ3')) {
				JHtml::_('jquery.framework');
					}
					else
					{
						$doc->addScript( '//ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js' );
					}
			}
			 if (!defined('SEOG_JS2')) {
			$doc->addScript(JURI::root(true).'/components/com_seoglossary/assets/js/jqeasytooltip.v1.3.js');
			$doc->addStyleSheet(JURI::root(true).'/components/com_seoglossary/assets/css/jqeasytooltip.css');
			$tooltip_style ="abbr
			{
			border-bottom: 1px dotted;
			}
			span .jqeasytooltip {
			 border-bottom: 1px dashed;
			}";
			  $doc->addStyleDeclaration($tooltip_style);
			     
			  $js="<script type='text/javascript'>function closeJQTip(id){ if(window.jQuery)
				{ jQuery('.jqeasytooltip'+id).jqeasytooltip(('close',{})); } }</script>";
			  $doc->addCustomTag($js);
			     define('SEOG_JS2', 1); 
			      }

		}
		

   
			$db = JFactory::getDbo();
			$nullDate = $db->getNullDate();
			$now = JFactory::getDate()->toSql();
			$search_text = $db->escape(strip_tags($content->text));
			$lang = JFactory::getLanguage();
			$query = "SELECT t.* FROM #__seoglossary t LEFT JOIN #__seoglossaries g ON t.catid = g.id WHERE t.state = 1 AND g.state = 1 AND ( LOWER('{$search_text}') LIKE CONCAT('%', LOWER(t.tterm), '%') OR '{$search_text}' REGEXP REPLACE(REPLACE(tsynonyms, ', ', '|'), ',', '|') ) AND ( g.language LIKE '" . $lang->getTag() ."' OR g.language = '*' ) AND (t.publish_up = ".$db->Quote($nullDate)." OR t.publish_up <= ".$db->Quote($now).") AND (t.publish_down = ".$db->Quote($nullDate)." OR t.publish_down >= ".$db->Quote($now).") ORDER BY ordering;";
			$db->setQuery($query);
			if (!($glossdata = $db->loadObjectList())) {
				if (JFactory::getApplication()->input->getInt('diagnose',0) == 1) {
					echo "<b>" . $db->getErrorMsg() . "</b><br />";
				}
			}
			
			$glossaryItems = array( );
			$a = new SeoglossaryHelper();
			$i = 0;
			if($found)
			{
			foreach ( $glossdata as $item )
			{
				$i++;
				$a = new SeoglossaryHelper( );
				$defination ="";
				if(@$item->icon)
				{
					$defination.="<img src='".$item->icon."'>";
				}
				$defination .=$a->processimage( $item->tdefinition );
					$link = SeoglossaryHelper::getUrlLink($item); 
				
		    if (!empty($item->tmore) && $com_params->get('show_read_more_tooltip', 0) == 1) {
			$format = '<div class="seog-tooltip-more-link"><a href="%s">' . $com_params->get('more_link', '...') . '</a></div>';
			$defination .= sprintf($format, JRoute::_($link));
		    }
	
		    if (JFactory::getApplication()->input->getInt('diagnose',0) == 1) {
			echo "<b>Looking for {$item->tterm}.</b><br />";
		    }
	
				$glossaryItems[] = array(
					'link' => $link,
			'phrase' => ($item->tterm),
					'explanation' => ($defination),
			'synonym_of' => 0,
			'show_tooltip'=>@$item->showtooltip,
				'target'=>@$item->target,
				'nofollow'=>@$item->nofollow
				);
				
				if ($com_params->get ('no_link', 0) == 1)
				{
					$idx = count($glossaryItems) - 1;
					$entry = &$glossaryItems[$idx];
					unset($entry['link']);
				}
	
		    $synonyms = explode(',', $item->tsynonyms);
		    foreach($synonyms as $syn)
		    {
			$syn = trim($syn);
			if (empty($syn))
			    continue;
	
			if (JFactory::getApplication()->input->getInt('diagnose',0) == 1) {
			    echo "<b>Looking for {$syn}.</b><br />";
			}
	
			$glossaryItems[] = array(
			    'link' => $link,
			    'phrase' => $syn,
			    'explanation' => ($defination),
			    'synonym_of' => @$item->tterm,
				'show_tooltip'=>@$item->showtooltip,
				'target'=>@$item->target,
				'nofollow'=>@$item->nofollow
			);
	
			if ($com_params->get ('no_link', 0) == 1)
			{
			    $idx = count($glossaryItems) - 1;
			    $entry = &$glossaryItems[$idx];
			    unset($entry['link']);
			}
		    }
	
			}
			
			
			$html = ($content->text);
			$replace="";
			$content->text = $a->replaceContext( $html, $glossaryItems );
			}
			$seogItems = array();
			if (preg_match_all($regex, $content->text, $matches, PREG_PATTERN_ORDER) > 0) {
			foreach ($matches[0] as $match) {
				$replace = strip_tags(preg_replace("/{.+?}/", "", $match));
					
			$content->text = preg_replace($regex, $replace, $content->text,1);
				}
			}
			
			if (preg_match_all($regex_enable, $content->text, $matches, PREG_PATTERN_ORDER) > 0) {
				$m=0;
				foreach ($matches[0] as $match) {
					$m++;
					$replace = strip_tags(preg_replace("/{.+?}/", "", $match));
					foreach ( $glossdata as $item )
					{
						if(strip_tags($item->tterm)==$replace)
						{
							$i++;
				$a = new SeoglossaryHelper( );
				$defination = $a->processimage( $item->tdefinition );
		   $link = SeoglossaryHelper::getUrlLink($item); 
	
		    if (!empty($item->tmore) && $com_params->get('show_read_more_tooltip', 0) == 1) {
			$format = '<div class="seog-tooltip-more-link"><a href="%s">' . $com_params->get('more_link', '...') . '</a></div>';
			$defination .= sprintf($format, JRoute::_($link));
		    }
	
		    if (JFactory::getApplication()->input->getInt('diagnose',0) == 1) {
			echo "<b>Looking for {$item->tterm}.</b><br />";
		    }
	
				$seogItems[] = array(
					'link' => $link,
			'phrase' => ($item->tterm),
					'explanation' => ($defination),
			'synonym_of' => 0,
				'show_tooltip'=>@$item->showtooltip,
				'target'=>@$item->target,
				'nofollow'=>@$item->nofollow
				);
				
				if ($com_params->get ('no_link', 0) == 1)
				{
					$idx = count($seogItems) - 1;
					$entry = &$seogItems[$idx];
					unset($entry['link']);
				}
	
		    $synonyms = explode(',', $item->tsynonyms);
		    foreach($synonyms as $syn)
		    {
			$syn = trim($syn);
			if (empty($syn))
			    continue;
	
			if (JFactory::getApplication()->input->getInt('diagnose',0) == 1) {
			    echo "<b>Looking for {$syn}.</b><br />";
			}
	
			$seogItems[] = array(
			    'link' => $link,
			    'phrase' => $syn,
			    'explanation' => ($defination),
			    'synonym_of' => @$item->tterm,
				'show_tooltip'=>@$item->showtooltip,
				'target'=>@$item->target,
				'nofollow'=>@$item->nofollow
			);
	
			if ($com_params->get ('no_link', 0) == 1)
			{
			    $idx = count($seogItems) - 1;
			    $entry = &$seogItems[$idx];
			    unset($entry['link']);
			}
			}
			
			$html = ($content->text);
			$replace="";
			
			$html = preg_replace($regex_enable, strip_tags($item->tterm), $html,1);
			$content->text = $a->replaceContext( $html, $seogItems);
			
						}			   
					}
			}
			}
			
			return true;
		}
	}
