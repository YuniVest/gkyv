<?php
/**
 * @package		Joomla & QuickForm
* @Copyright ((c) juice-lab.ru
        * @license    GNU/GPL
        */
defined('_JEXEC') or die();
class qfFilds
{
    public $calculator = false;
    public $submited = false;
    public $iscart = false;
    public $errormes = false;
    public $project = false;
    public $redirect = false;
    protected $child = array();
    public $back = false;

    public function __construct()
    {
        $this->app = JFactory::getApplication();
        $this->db = JFactory::getDBO();
        $this->ajaxform = $this->app->input->get('task')=='ajax';
    }

    public function getResultHtml($project)
    {
        $data = $this->getMainForm($project->id);

        if($project->tmpl == 'json'){
        	$html = $this->jsonTmpl($project, $data);
        }
        elseif($project->tmpl == 'simple'){
        	$html = $this->simplTmpl($project, $data);
        }
        else{
        	$html = $this->defaultTmpl($project, $data);
        }
        return $html;
    }


    public function mlangLabel($val) {
      if(strpos($val, 'QF_')===0) return JText::_($val);
      return $val;
    }

    public function qfcheckToken()
    {
      JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
      $token = explode('/', str_replace(array('w', '.', '-', '|'), '', JURI::root()));
      if($token[2] != $this->app->input->get('qftoken', '', 'string')) jexit(JText::_('JINVALID TOKEN'));
    }

    public function getProjectById($id)
    {
      if($this->project) return $this->project;

      $lang = JFactory::getLanguage();
      $user = JFactory::getUser();
      $groups = implode(',', $user->getAuthorisedViewLevels());
      $this->db->setQuery('SELECT * FROM #__quickform_projects' . ' WHERE published=1' . ' AND (language=' . $this->db->quote($lang->getTag()) . ' OR language=' . $this->db->quote('*') . ')' . ' AND access IN (' . $groups . ')' . ' AND id = ' . ( int ) $id);
      $this->project = $this->db->loadObject();

      $this->project->params = json_decode($this->project->params);
      if(isset($this->project->params->languagelink) && $this->project->params->languagelink){
        $lang->load($this->project->params->languagelink);
      }

      return $this->project;
    }

    public function getMainForm($projectid)
    {
        $db = JFactory::getDBO();
        $data = array();

        $db->setQuery('SELECT * FROM #__quickform_forms
			WHERE def=1
			AND projectid = ' . ( int ) $projectid);
        $form = $db->loadObject();
        if (! empty($form)) {
            $data = $this->getFilds($form);
        }
        return $data;
    }

    protected function getChildren($id)
    {
        if (! isset($this->child [$id])) {
            $this->db->setQuery('SELECT * FROM #__quickform_forms
				WHERE id = ' . ( int ) $id);
            $this->child [$id] = $this->db->loadObject();
        }

        return $this->getFilds($this->child [$id]);
    }

    protected function getFilds($form)
    {
        $data = array();
        if (! $form) {
            return $data;
        }
        $fields = json_decode($form->fields);
            foreach ($fields as $field) {
                $field->fildid = $form->id . '.' . $field->fildnum;
                unset($field->fildnum);

                switch ($field->teg) {
                    case 'select':
                        $data [] = $this->getSelect($field);
                        break;
                    case 'input[radio]':
                        $data [] = $this->getRadio($field);
                        break;
                    case 'input[checkbox]':
                        $data [] = $this->getCheckbox($field);
                        break;
                    case 'textarea':
                        $data [] = $this->getTextarea($field);
                        break;
                    case 'customHtml':
                        $data [] = $this->getCustomHtml($field);
                        break;
                    case 'customPhp':
                        $data [] = $this->getCustomPhp($field);
                        break;
                    case 'calculatorSum':
                        $data [] = $this->getCalculatorSum($field);
                        break;
                    case 'recaptcha':
                        $this->getRecaptcha($field);
                        break;
                    case 'submit':
                        $this->submited = true;
                        $this->getRedirect($field);
                        break;
                    case 'backemail':
                        $data [] = $this->getBackemail($field);
                        break;
                    case 'cloner':
                        $data [] = $this->getCloner($field);
                        break;
                    case 'calcCondition':
                        $data [] = $this->getCondition($field);
                        break;
                    case 'qftabs':
                        $data [] = $this->getTabs($field);
                        break;
                    case 'addToCart':
                        $this->iscart = true;
                        break;
                    case 'input[file]':
                        $data [] = $this->getFile($field);
                        break;
                    case 'input[hidden]':
                        $data [] = $this->getHidden($field);
                        break;
                    case 'input[text]':
                        $data [] = $this->getText($field);
                        break;
                    case 'input[color]':
                        $data [] = $this->getColor($field);
                        break;
                    case 'input[date]':
                        $data [] = $this->getDate($field);
                        break;
                    case 'input[email]':
                        $data [] = $this->getEmail($field);
                        break;
                    case 'input[number]':
                        $data [] = $this->getNumber($field);
                        break;
                    case 'input[range]':
                        $data [] = $this->getRange($field);
                        break;
                    case 'input[tel]':
                        $data [] = $this->getTel($field);
                        break;
                    case 'input[url]':
                        $data [] = $this->getUrl($field);
                        break;
                    case 'input[button]':
                    case 'input[reset]':
                        break;
                    default:
                        $data [] = $field;
                }
            }
        return $data;
    }

    protected function chekRequired($field, $val)
    {
        if (isset($field->required) && $field->required) {
            if (! $val) {

              if($field->label){
                $err = $this->mlangLabel($field->label);
              }
              else $err = $field->teg;

              $this->errormes[] = JText::_('COM_QF_NOT_ALL') . ': '. $err;
            }
        }
    }

    protected function get($name, $i)
    {
        if (isset($_POST [$name] [$i])) {
            return $_POST [$name] [$i];
        } else {
            $this->errormes[] = JText::_('FORM_ERROR') . '_' . $name;
        }
    }

    protected function getUrl($field)
    {
        static $i = 0;

        $val = $this->get('qfurl', $i);
        $val = strip_tags($val);

        $field->value = $val;
        $this->chekRequired($field, $val);
        $i ++;

        return $field;
    }

    protected function getTel($field)
    {
        static $i = 0;

        $val = $this->get('qftel', $i);
        $val = strip_tags($val);

        $field->value = $val;
        $this->chekRequired($field, $val);
        $i ++;

        return $field;
    }

    protected function getRange($field)
    {
        static $i = 0;

        $val = ( int ) $this->get('qfrange', $i);

        $field->value = $val;
        $this->chekRequired($field, $val);
        $i ++;

        return $field;
    }

    protected function getNumber($field)
    {
        static $i = 0;

        $val = ( int ) $this->get('qfnumber', $i);

        $field->value = $val;
        $this->chekRequired($field, $val);
        $i ++;

        return $field;
    }

    protected function getEmail($field)
    {
        static $i = 0;

        $val = $this->get('qfemail', $i);
        $val = strip_tags($val);

        $field->value = $val;
        $this->chekRequired($field, $val);
        $i ++;

        return $field;
    }

    protected function getDate($field)
    {
        static $i = 0;

        $val = $this->get('qfdate', $i);
        $val = strip_tags($val);

        $field->value = $val;
        $this->chekRequired($field, $val);
        $i ++;

        return $field;
    }

    protected function getColor($field)
    {
        static $i = 0;

        $val = $this->get('qfcolor', $i);
        $val = strip_tags($val);

        $field->value = $val;
        $this->chekRequired($field, $val);
        $i ++;

        return $field;
    }

    protected function getText($field)
    {
        static $i = 0;

        $val = $this->get('qftext', $i);
        $val = strip_tags($val);

        $field->value = $val;
        $this->chekRequired($field, $val);
        $i ++;

        return $field;
    }

    protected function getHidden($field)
    {
        static $i = 0;

        $val = $this->get('qfhidden', $i);
        $val = strip_tags($val);

        $field->value = $val;
        $this->chekRequired($field, $val);
        $i ++;

        return $field;
    }

    protected function getFile($field)
    {
        static $i = 0;

        $val = ( int ) $this->get('qffile', $i);

        $files = $this->app->input->files->get('inpfile', array(), 'array');
        $field->value = '';
        if (isset($files [$i] ['name'])) {
            $field->value = $files [$i] ['name'];
        }

        $this->chekRequired($field, $field->value);
        $i ++;

        return $field;
    }

    protected function getCloner($field)
    {
        static $i = 0;

        $val = ( int ) $this->get('qfcloner', $i);

        $max = $field->max;
        $id = ( int ) $field->related;
        if ($max && $val > $max) {
            $this->errormes[] = JText::_('FORM_ERROR') . '_qfcloner_max';
        }
        $field->value = $val;
        $field->data = array();
        $i ++;

        for ($n = 0; $n < $val; $n ++) {
            $field->data [] = $this->getChildren($id);
        }

        return $field;
    }

    protected function getTabs($field)
    {
        foreach($field->options as $option){
          if ($id = ( int ) $option->related) {
                $field->data [] = $this->getChildren($id);
          }
          else {
                $field->data [] = array();
          }
        }

        return $field;
    }


    protected function getCondition($field)
    {
        if (isset($field->related) && $id = ( int ) $field->related) {
              $field->data = $this->getChildren($id);
        }

        return $field;
    }

    protected function getRedirect($field)
    {
        if (isset($field->redirect) && $field->redirect) {
              $this->redirect = $field->redirect;
        }
    }



    protected function getBackemail($field)
    {
        if (isset($field->reg) && $field->reg) {
            $user = JFactory::getUser();
            $field->back = $user->get('email');
        } else {
            $field->back = $this->get('qfemail', 0);
        }

        $el = $this->app->input->get('qfbackemail');
        $field->value = $el;

        if($field->value && $field->back) {
          $this->back = $field->back;
        }

        $field->hide = 1;
        $this->chekRequired($field, $el);
        return $field;
    }

    protected function getRecaptcha($field)
    {
        $user = JFactory::getUser();
        $onlyguest = (isset($field->show) && $field->show) ? 1 : 0;
        if ($user->get('guest') || ! $onlyguest) {
            $captchaplugin = JPluginHelper::getPlugin('captcha', 'recaptcha');
            $captchaparams = new JRegistry();
            $captchaparams->loadString($captchaplugin->params);

            $privatekey = $captchaparams->get('private_key');
            $remoteip = $this->app->input->server->get('REMOTE_ADDR', '', 'string');

            $challenge = null;
            $response = $this->app->input->get('g-recaptcha-response', '', 'string');
            $spam = ($response == null || strlen($response) == 0);

            if (empty($privatekey) || empty($remoteip) || $spam) {
                $this->errormes[] = JText::_('RECAPTCHA_ERROR');
            }
            require_once JPATH_BASE . '/plugins/captcha/recaptcha/recaptchalib.php';
            $reCaptcha = new JReCaptcha($privatekey);
            $response = $reCaptcha->verifyResponse($remoteip, $response);

            if (! isset($response->success) || ! $response->success) {
              if (is_array($response->errorCodes))
              {
                foreach ($response->errorCodes as $error)
                {
                  $this->errormes[] = $error;
                }
              }
            }
        }
    }

    protected function getCalculatorSum($field)
    {
        $this->calculator = true;
        $field->hide = 1;
        $field->label = (isset($field->label) && $field->label) ? $this->mlangLabel($field->label) : '';
        $field->unit = (isset($field->unit) && $field->unit) ? $this->mlangLabel($field->unit) : '';
        $field->pos = (isset($field->pos) && $field->pos) ? $field->pos : '';
        return $field;
    }

    protected function getCustomHtml($field)
    {
        return $field;
    }

    protected function getCustomPhp($field)
    {
        $cod = (isset($field->customphp2) && $field->customphp2) ? $field->customphp2 : '';
        $config = JFactory::getConfig();
        $tmpfname = tempnam($config->get('tmp_path'), "qf");
        $handle = fopen($tmpfname, "w");
        fwrite($handle, $cod, strlen($cod));
        fclose($handle);
        $field->value = $this->get_include_contents($tmpfname);
        unlink($tmpfname);
        return $field;
    }

    protected function get_include_contents($filename)
    {
        if (is_file($filename)) {
            ob_start();
            include $filename;
            return ob_get_clean();
        }
        return false;
    }

    protected function getTextarea($field)
    {
        static $i = 0;

        $val = $this->get('qftextarea', $i);
        $val = nl2br(strip_tags($val, '<a></a>'));
        $field->value = $val;
        $this->chekRequired($field, $val);
        $i ++;

        return $field;
    }

    protected function getCheckbox($field)
    {
        static $i = 0;

        $val = ( int ) $this->get('qfcheckbox', $i);
        $field->value = $val;
        $this->chekRequired($field, $val);

        if(isset($field->hidech) && $field->hidech){
          if(!$val) $field->hide = 1;
        }

        $i ++;

        if (isset($field->related) && $id = ( int ) $field->related) {
            if ($field->value) {
                $field->data = $this->getChildren($id);
            }
        }

        return $field;
    }

    protected function getRadio($field)
    {
        static $i = 0;

        $val = ( int ) $this->get('qfradio', $i);
        $this->chekRequired($field, $val);

        $option = $field->options [$val];

        $field->calculator = (isset($option->calculator) && $option->calculator) ? $option->calculator : '';
        $field->value = $option->label;
        unset($field->options);
        $i ++;

        $related = (isset($option->related) && $option->related) ? $option->related : '';
        if ($id = ( int ) $related) {
            $field->data = $this->getChildren($id);
        }

        return $field;
    }

    protected function getSelect($field)
    {
        static $i = 0;

        $val = ( int ) $this->get('qfselect', $i);
        $this->chekRequired($field, $val);

        $option = $field->options [$val];

        $field->calculator = (isset($option->calculator) && $option->calculator) ? $option->calculator : '';
        $field->value = $option->label;
        unset($field->options);
        $i ++;

        $related = (isset($option->related) && $option->related) ? $option->related : '';
        if ($id = ( int ) $related) {
            $field->data = $this->getChildren($id);
        }

        return $field;
    }

    public function simplTmpl($project, $data)
    {
        $html = '';
        if (! $project->showtitle) {
            $html .= '<h1>' . $this->mlangLabel($project->title) . '</h1>';
        }
        $html .= $this->getSimplRows($data);
        if ($this->calculator) {
            if (trim($project->calculator)) {
                $sum = $this->getFormulSum($project, $data);
                $i = 0;
                foreach ($sum as $arr) {
                    if ($arr [1]) {
                        $len = sizeof($arr [1]);
                    }
                    if ($i < $len) {
                        if ($arr [1] [$i] [0]) {
                            $html .= $arr [1] [$i] [0] . ' : ';
                        }
                        if ($arr [1] [$i] [2]) {
                            $html .= $arr [0] . ' ' . $arr [1] [$i] [1] . '<br>';
                        } else {
                            $html .= $arr [1] [$i] [1] . ' ' . $arr [0] . '<br>';
                        }
                    }
                    $i ++;
                }
            } else {
                $sum = $this->getSum($project, $data);
                if ($sum [0]) {
                    $html .= $sum [0] . ' : ';
                }
                if ($sum [2]) {
                    $html .= $sum [3] . ' ' . $sum [1] . '<br>';
                } else {
                    $html .= $sum [1] . ' ' . $sum [3] . '<br>';
                }
            }
        }
        return $html;
    }

    protected function getSimplRows($data)
    {
        $html = '';
        foreach ($data as $fild) {
            if (! isset($fild->hide) || ! $fild->hide) {
                if ($fild->teg == 'cloner') {
                    foreach ($fild->data as $row) {
                        $html .= $this->getSimplRows($row);
                    }
                } elseif ($fild->teg == 'qftabs') {
                      $options = $fild->options;
                      for ($n = 0; $n < sizeof($options); $n ++) {
                          $html .= '<br>' . $this->mlangLabel($options[$n]->label) . '<br>';
                          $html .= $this->getSimplRows($fild->data[$n]);
                      }
                } elseif ($fild->teg == 'customHtml') {
                    $html .= $this->mlangLabel($fild->label) . '<br>';
                } elseif ($fild->teg == 'customPhp') {
                    if ($fild->label) {
                        $html .= $this->mlangLabel($fild->label) . '<br>';
                    }
                    $html .= $this->mlangLabel($fild->value) . '<br>';
                } else {
                    $html .= $this->mlangLabel($this->letLable($fild)) . ' : ';
                    if ($fild->teg == 'input[checkbox]') {
                        $html .= $fild->value ? (JText::_('JYES') . '<br>') : (JText::_('JNO') . '<br>');
                    } else {
                        $html .= $this->mlangLabel($fild->value) . '<br>';
                    }
                    if (isset($fild->data) && ! empty($fild->data)) {
                        $html .= $this->getSimplRows($fild->data);
                    }
                }
            }
        }
        return $html;
    }

    public function jsonTmpl($project, $data)
    {
        $html = '';
        if (! $project->showtitle) {
            $html .= '<h1>' . $this->mlangLabel($project->title) . '</h1>';
        }
        if ($this->calculator) {
            if (trim($project->calculator)) {
                $data ['sum'] = $this->getFormulSum($project, $data);
            } else {
                $data ['sum'] = $this->getSum($project, $data);
            }
        }
        $html .= json_encode($data);
        return $html;
    }


    public function getSumInArr($project, $data)
    {
        if ($this->calculator) {
            if (trim($project->calculator)) {
                $arr = $this->getFormulSum($project, $data);
                foreach($arr as $k=>$ar){
                  $arr[$k][0] = str_replace(array(',', ' '), array('.', ''), $ar[0]);
                }
            } else {
                $arr= $this->getSum($project, $data);
                $arr['sum'] = str_replace(array(',', ' '), array('.', ''), $arr[3]);
            }

            return $arr;
        }

        return array();
    }



    public function defaultTmpl($project, $data)
    {
        $html = '';
        if (! $project->showtitle) {
            $html .= '<h1>' . $this->mlangLabel($project->title) . '</h1>';
        }
        $html .= '<table border="1" width="100%" style="max-width:800px" cellpadding="1" cellspacing="0">';
        $html .= $this->getRows($data);
        if ($this->calculator) {
            if (trim($project->calculator)) {
                $sum = $this->getFormulSum($project, $data);
                $html .= $this->getTrSumFormul($sum);
            } else {
                $sum = $this->getSum($project, $data);
                $html .= $this->getTrSum($sum);
            }
        }
        $html .= '</table>';
        return $html;
    }

    protected function getRows($data)
    {
        $html = '';
        foreach ($data as $fild) {
            if (! isset($fild->hide) || ! $fild->hide) {
                if ($fild->teg == 'cloner') {
                    $html .= $this->getRowsCloner($fild);
                } elseif ($fild->teg == 'qftabs') {
                      $options = $fild->options;
                      for ($n = 0; $n < sizeof($options); $n ++) {
                          $html .= '<tr><td colspan="2">' . $this->mlangLabel($options[$n]->label) . '</td></tr>';
                          $html .= $this->getRows($fild->data[$n]);
                      }
                } elseif ($fild->teg == 'customHtml') {
                    $html .= '<tr><td colspan="2">' . $this->mlangLabel($fild->label) . '</td></tr>';
                } elseif ($fild->teg == 'customPhp' && !$fild->label) {
                    $html .= '<tr><td colspan="2">' . $this->mlangLabel($fild->value) . '</td></tr>';
                } elseif ($fild->teg == 'calcCondition') {
                    $html .= $this->getRows($fild->data);
                } else {
                    $html .= $this->getTr($fild);
                    if (isset($fild->data) && ! empty($fild->data)) {
                        $html .= $this->getRows($fild->data);
                    }
                }
            }
        }
        return $html;
    }

    protected function letLable($field)
    {
      if(!$field->label){
        if(isset($field->placeholder) && $field->placeholder){
          return $field->placeholder;
        }
      }
      return $field->label;
    }


    protected function getTr($fild)
    {
        $html = '';
        $html .= '<tr>';
        $html .= '<td style="padding-left:10px" width="62%">' . $this->mlangLabel($this->letLable($fild)) . '</td>';
        $html .= $this->getTdVal($fild);
        $html .= '</tr>';
        return $html;
    }

    protected function getTrSum($sum)
    {
        $html = '';
        $html .= '<tr>';
        $html .= '<td align="right" style="padding:10px">' . $sum [0] . '</td>';
        if ($sum [2]) {
            $html .= '<td style="padding:10px">' . $sum [3] . ' ' . $sum [1] . '</td>';
        } else {
            $html .= '<td style="padding:10px">' . $sum [1] . ' ' . $sum [3] . '</td>';
        }
        $html .= '</tr>';
        return $html;
    }

    protected function getTdVal($fild)
    {
        $html = '';
        if ($fild->teg == 'input[checkbox]') {
            $html .= '<td style="padding-left:10px">' . ($fild->value ? (JText::_('JYES')) : (JText::_('JNO'))) . '</td>';
        } else {
            $html .= '<td style="padding-left:10px">' . $this->mlangLabel($fild->value) . '</td>';
        }
        return $html;
    }

    protected function getRowsCloner($fild)
    {
        $html = '';
        if (! empty($fild->data)) {
            $html .= '<tr><td colspan="2">';
            $html .= '<table border="1" width="100%" cellpadding="1" cellspacing="0">';
            if (! $fild->orient) {
                // horizontally cloner
                $i = 0;
                foreach ($fild->data as $row) {
                    if (! $i) {
                        $html .= '<tr>';
                        foreach ($row as $item) {
                            if (! isset($item->hide) || ! $item->hide) {
                                $html .= '<th>' . $this->mlangLabel($item->label) . '</th>';
                            }
                        }
                        $i ++;
                        $html .= '</tr>';
                    }
                    $html .= '<tr>';
                    foreach ($row as $item) {
                        if (! isset($item->hide) || ! $item->hide) {
                            $html .= $this->getTdVal($item);
                        }
                    }
                    $html .= '</tr>';
                }
            } else {
                // vertically cloner
                foreach ($fild->data as $row) {
                    $html .= '<tr><td colspan="2">';
                    $html .= '<table border="1" width="100%" cellpadding="1" cellspacing="0">';
                    $html .= $this->getRows($row);
                    $html .= '</table>';
                    $html .= '</td></tr>';
                }
            }
            $html .= '</table>';
            $html .= '</td></tr>';
        }
        return $html;
    }

    public function getSum($project, $data)
    {
        $str = '';
        $this->sumdata = false;
        $str .= $this->getSumFrom($data);

        $str = str_replace(',', '.', $str);
        if(!$str) $str = 0;

        // $str = preg_replace('/[^0-9()-.+\*\/]/', '', $str);
        try {
            $sum = eval('$res=(' . $this->checkStr($str) . ');return $res;');
        } catch (Exception $e) {
            $sum = 'error';
        }

        if (( int ) $sum != $sum) {
            $sum = round($sum, 2);
        }

        $req = $_POST ['qfprice'];
        if ($sum != $req [0]) {
            $this->errormes[] = JText::_('COM_QF_EMAIL_ERROR');
        }

        if (( int ) $sum != $sum) {
            $sum = number_format($sum, 2, ',', ' ');
        } else {
            $sum = number_format($sum, 0, '', ' ');
        }

        list($label, $unit, $after) = $this->sumdata;

        return array(
                            $label,
                            $unit,
                            $after,
                            $sum
                    );
    }

    protected function checkStr($str) {
      return preg_replace('/[^0-9()-.+\*\/]/', '', $str);
    }

    public function getSumFrom($data)
    {
        $str = '';
        foreach ($data as $fild) {
            if ($fild->teg == 'cloner') {
                $str .= $fild->clonerstart;
                foreach ($fild->data as $row) {
                    $str .= $this->getSumFrom($row);
                }
                $str .= $fild->clonerend;
            }
            elseif($fild->teg == 'qftabs'){
              foreach ($fild->data as $row) {
                  $str .= $this->getSumFrom($row);
              }
            }
            elseif ($fild->teg == 'calcCondition') {
                $boxstr = $this->getSumFrom($fild->data);
                $boxres = '';
                $condition = (isset($fild->condition) && $fild->condition) ? $fild->condition : '';

                if($condition){
                  try {
                      $boxsum = eval('$res=(' . $this->checkStr($boxstr) . ');return $res;');
                  } catch (Exception $e) {
                      $boxsum = 'error';
                  }

                  $condition = str_replace('s', round($boxsum,10), $condition);
                  $boxres = eval('$res=(' . $condition . ');return $res;');

                }

                if($boxres){
                  $clonerstart = (isset($fild->clonerstart) && $fild->clonerstart) ? $fild->clonerstart : '';
                  $clonerend = (isset($fild->clonerend) && $fild->clonerend) ? $fild->clonerend : '';
                  $str .= $clonerstart;
                  $str .= $boxstr;
                  $str .= $clonerend;
                }
                else{
                  $str .= $boxstr;
                }
            }
             elseif ($fild->teg == 'calculatorSum') {
                $this->sumdata = array(
                        $fild->label,
                        $fild->unit,
                        $fild->pos
                );
            } else {
                if (isset($fild->calculator) && $fild->calculator) {
                    if ($fild->teg == 'input[checkbox]') {
                        if ($fild->value) {
                            $str .= $fild->calculator;
                        }
                    } else {
                        $str .= str_replace('v', $fild->value, $fild->calculator);
                    }
                }
                if (isset($fild->data) && ! empty($fild->data)) {
                    $str .= $this->getSumFrom($fild->data);
                }
            }
        }
        return $str;
    }

    protected function getFormulSum($project, $data)
    {
        $strarr = array();
        $find = array(
                '',
                array()
        );
        $formuls = explode(';', trim($project->calculator));

        foreach ($formuls as $formul) {
            $pats = explode('=', trim($formul));

            if (sizeof($pats) == 2) {
                $num = trim(substr($pats [0], 3));
                $pat = trim($pats [1]);

                $patfields = explode('|', $pat);
                $str = '';

                foreach ($patfields as $patfield) {
                    if ($patfield) {
                        $find = $this->findFildFromData($patfield, $data);
                        $str .= $find [0];
                    }
                }

                $str = str_replace(',', '.', $str);
                if(!$str) $str = 0;
                try {
                    $sum = eval('$res=(' . $this->checkStr($str) . ');return $res;');
                } catch (Exception $e) {
                    $sum = 'error';
                }

                if (( int ) $sum != $sum) {
                    $sum = round($sum, 2);
                }

                $req = $_POST ['qfprice'];
                if (isset($find [1] [$num]) && $sum != $req [$num]) {
                    $this->errormes[] = JText::_('COM_QF_EMAIL_ERROR_SUM');
                }

                if (( int ) $sum != $sum) {
                    $sum = number_format($sum, 2, ',', ' ');
                } else {
                    $sum = number_format($sum, 0, '', ' ');
                }

                $strarr [$num] = array(
                                    $sum,
                                    $find [1]
                            );
            }
        }

        return $strarr;
    }

    protected function findFildFromData($patfield, $data)
    {
        $str = '';
        $sumdata = array();

        foreach ($data as $fild) {
            if (isset($fild->fildid) && $fild->fildid == $patfield) {
                if (isset($fild->calculator) && $fild->calculator) {
                    if ($fild->teg == 'input[checkbox]') {
                        if ($fild->value) {
                            $str .= $fild->calculator;
                        }
                    } else {
                        $str .= str_replace('v', $fild->value, $fild->calculator);
                    }
                }
            }

            if ($fild->teg == 'cloner') {
                foreach ($fild->data as $row) {
                    $find = $this->findFildFromData($patfield, $row);
                    $str .= $find [0];
                }
            }
            elseif($fild->teg == 'qftabs'){
              foreach ($fild->data as $row) {
                  $find = $this->findFildFromData($patfield, $row);
                  $str .= $find [0];
              }
            }
             elseif ($fild->teg == 'calculatorSum') {
                $sumdata [] = array(
                        $fild->label,
                        $fild->unit,
                        $fild->pos
                );
            } else {
                if (isset($fild->data) && ! empty($fild->data)) {
                    $find = $this->findFildFromData($patfield, $fild->data);
                    $str .= $find [0];
                }
            }
        }

        return array(
                $str,
                $sumdata
        );
    }

    protected function getTrSumFormul($sum)
    {
        $html = '';
        $i = 0;
        foreach ($sum as $arr) {
            if ($arr [1]) {
                $len = sizeof($arr [1]);
            }
            if ($i < $len) {
                $html .= '<tr>';
                $html .= '<td align="right" style="padding-right:10px">' . $arr [1] [$i] [0] . '</td>';
                if ($arr [1] [$i] [2]) {
                    $html .= '<td style="padding-left:10px">' . $arr [0] . ' ' . $arr [1] [$i] [1] . '</td>';
                } else {
                    $html .= '<td style="padding-left:10px">' . $arr [1] [$i] [1] . ' ' . $arr [0] . '</td>';
                }
                $html .= '</tr>';
            }
            $i ++;
        }

        return $html;
    }

    public function writeStat($project, $html)
    {
        if ($project->params) {
            if (! $project->params->history) {
                return true;
            }
        }

        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $query = $db->getQuery(true);
        $columns = array(
                'st_formid',
                'st_date',
                'st_form',
                'st_title',
                'st_ip',
                'params',
                'st_user',
                'st_status'
        );
        $values = array(
                $project->id,
                $db->quote(gmdate('Y-m-d H:i:s')),
                $db->quote($html),
                $db->quote($this->mlangLabel($project->title)),
                $db->quote($this->getip()),
                '""',
                $user->get('id'),
                0
        );
        $query->insert($db->quoteName('#__quickform_ps'))->columns($db->quoteName($columns))->values(implode(',', $values));

        $db->setQuery($query);
        $res1 = $db->execute();

        $db->setQuery("UPDATE `#__quickform_projects` SET hits = ( hits + 1 ) WHERE id = " . ( int ) $project->id);
        $res2 = $db->execute();

        if ($res1 && $res2) {
            return true;
        }
        return false;
    }

    protected function getip()
    {
        if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown")) {
            $ip = getenv("HTTP_CLIENT_IP");
        } elseif (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown")) {
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        } elseif (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown")) {
            $ip = getenv("REMOTE_ADDR");
        } elseif (! empty($_SERVER ['REMOTE_ADDR']) && strcasecmp($_SERVER ['REMOTE_ADDR'], "unknown")) {
            $ip = $_SERVER ['REMOTE_ADDR'];
        } else {
            $ip = "unknown";
        }

        return ($ip);
    }

    public function sendMail($project, $html)
    {
        $mailfrom = $this->app->get('mailfrom');
        $fromname = $this->app->get('fromname');
        $sitename = $this->app->get('sitename');

        $mail = JFactory::getMailer();

        if ($project->toemail) {
            $arr = explode(',', $project->toemail);
            foreach ($arr as $ar) {
                $mail->addRecipient(trim($ar));
            }
        } else {
            $mail->addRecipient($mailfrom);
        }

        if ($this->back) {
            $mail->addRecipient(JStringPunycode::emailToPunycode($this->back));
        }

        $mail->setSender(array(
                    $mailfrom,
                    $fromname
            ));
        $mail->addReplyTo($mailfrom, $fromname);

        if ($project->subject) {
            $mail->setSubject($this->mlangLabel($project->subject));
        } else {
            $mail->setSubject($this->mlangLabel($project->title));
        }

        $mail->setBody($html);
        $mail->isHTML(true);

        $files = $this->app->input->files->get('inpfile', array(), 'array');
        foreach ($files as $file) {
            if (isset($file ['name']) && $file ['tmp_name'] && $file ['name']) {
                $mail->addAttachment($file ['tmp_name'], $file ['name']);
            }
        }
        return $mail->Send();
    }
}
