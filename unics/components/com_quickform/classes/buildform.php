<?php
/**
 * @package		Joomla & QuickForm
* @Copyright ((c) juice-lab.ru
        * @license    GNU/GPL
        */
defined('_JEXEC') or die();
class QuickForm
{
  public function __construct()
  {
      $this->lang = JFactory::getLanguage();
      $this->lang->load('com_quickform');
      $this->db = JFactory::getDBO();
  }

    public function getQuickForm($id)
    {
        $html = '';
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $groups = $user->getAuthorisedViewLevels();

        $ajax = $app->input->get('task')=='ajax';
        $project = $this->getProjectById($id);

        if (! empty($project)) {
            if ($project->published && in_array($project->access, $groups)) {
                if ($project->language == $this->lang->getTag() || $project->language == '*') {

                    if(!$ajax){

                      JHtml::_('jquery.ui');

                      if ($project->cssstyle != 'none') {
                        JHtml::_('stylesheet', 'components/com_quickform/assets/css/' . $project->cssstyle, array('version' => 'auto'));
                      }

                      JHtml::_('script', 'components/com_quickform/assets/js/quickform.js', array('version' => 'auto'));
                    }

                    if ($project->params->modal) {
                      if(!$ajax){
                        $html .= '<a href="javascript:void(0);" class="qf2modal" data-project="'.$project->id.'">'.$this->mlangLabel($project->params->modallink).'</a>';

                        return $html;
                      }
                    }


                    $this->db->setQuery('SELECT * FROM #__quickform_forms WHERE def=1 AND projectid = ' . ( int ) $project->id);
                    $form = $this->db->loadObject();

                    if (! empty($form)) {
                        $html .= $this->getFilds($form);
                    }
                }
            }
        } else {
            return 'form id = ' . $id . ' not found';
        }

        if ($html) {
            $rethtml = '';

            $rethtml .=  '<div class="qf2form"><form method="post" enctype="multipart/form-data">' . $html . '<input name="option" type="hidden" value="com_quickform" /><input name="id" type="hidden" value="' . $id . '" />' . JHtml::_('form.token');

            if ($project->calculator) {
                $rethtml .= '<input name="formul" type="hidden" value="' . trim($project->calculator) . '" />';
            }

            if ($project->params->ajaxform) {
                $rethtml .= '<input name="task" type="hidden" value="ajax" /><input name="mod" type="hidden" value="qfajax" />';
            }
            else{
              $rethtml .= '<input name="task" type="hidden" value="" /><input name="mod" type="hidden" value="" />';
            }

            if (isset($project->params->qfkeepalive) && $project->params->qfkeepalive) {
              $config= new JConfig();
              $qfkeepalive = 60000*(($config->lifetime)-0.5);
            }
            else{
              $qfkeepalive = 0;
            }
            $rethtml .= '<input name="qfkeepalive" type="hidden" value="'.$qfkeepalive.'" class="qfkeepalive" />';

            $rethtml .= '<input name="root" type="hidden" value="' . JURI::root() . '" />' . $this->getQFlink() . '</form></div>';

            return $rethtml;
        }
    }

    protected function getDataById($id)
    {
        $this->db->setQuery('SELECT * FROM #__quickform_forms WHERE id = ' . ( int ) $id);
        return $this->db->loadObject();
    }

    protected function getProjectById($id)
    {
        $this->db->setQuery('SELECT * FROM #__quickform_projects WHERE id = ' . ( int ) $id);
        $this->project = $this->db->loadObject();
        if($this->project){
          $this->project->params = json_decode($this->project->params);
          if(isset($this->project->params->languagelink) && $this->project->params->languagelink){
            $this->lang->load($this->project->params->languagelink);
          }
        }
        return $this->project;
    }


    protected function getFilds($form)
    {
        $html = '';
        $fields = json_decode($form->fields);
        $id = $form->id;

        foreach ($fields as $field) {
            switch ($field->teg) {
                case 'select':
                    $html .= $this->qSelect($field, $id);
                    break;
                case 'input[radio]':
                    $html .= $this->qRadio($field, $id);
                    break;
                case 'input[checkbox]':
                    $html .= $this->qCheckbox($field, $id);
                    break;
                case 'input[file]':
                    $html .= $this->qFile($field, $id);
                    break;
                case 'textarea':
                    $html .= $this->qTextarea($field, $id);
                    break;
                case 'submit':
                    $html .= $this->qSubmit($field, $id);
                    break;
                case 'customHtml':
                    $html .= $this->qCustomHtml($field, $id);
                    break;
                case 'customPhp':
                    $html .= $this->qCustomPhp($field, $id);
                    break;
                case 'calculatorSum':
                    $html .= $this->qCalculatorSum($field, $id);
                    break;
                case 'recaptcha':
                    $html .= $this->qRecaptcha($field, $id);
                    break;
                case 'backemail':
                    $html .= $this->qBackemail($field, $id);
                    break;
                case 'cloner':
                    $html .= $this->qCloner($field, $id);
                    break;
                case 'calcCondition':
                    $html .= $this->qCalcCondition($field, $id);
                    break;
                case 'addToCart':
                    $html .= $this->qAddToCart($field, $id);
                    break;
                case 'qftabs':
                    $html .= $this->qTabs($field, $id);
                    break;
                default:
                    $html .= $this->qInput($field, $id);
            }
        }
        return $html;
    }

    protected function mlangLabel($val) {
      if(strpos($val, 'QF_')===0) return JText::_($val);
      return $val;
    }

    protected function get($val, $field) {
      if(isset($field->$val) && $field->$val) return true;
      return false;
    }

    protected function getLabel($field) {
      $html = '';

      if($field->label){
        $html .= '<label class="qf2label">' . $this->mlangLabel($field->label) . ($this->get('required', $field) ? ' <span class="qf2labelreq">*</span>' : '') . '</label>';
      }
      else{
        $html .= '<label class="qf2label"></label>';
      }
      return $html;
    }




    protected function qCustomPhp($field, $id)
    {
        $cod = $this->get('customphp1', $field) ? $field->customphp1 : '';
        $config = JFactory::getConfig();
        $tmpfname = tempnam($config->get('tmp_path'), "qf");
        $handle = fopen($tmpfname, "w");
        fwrite($handle, $cod, strlen($cod));
        fclose($handle);
        $res = $this->get_include_contents($tmpfname);
        unlink($tmpfname);

        $html = '';
        $html .= '<div class="qf2">';
        if ($field->label) {
            $html .= '<label class="qf2label">' . $this->mlangLabel($field->label) . '</label>';
        }
        $html .= $res;
        $html .= '</div>';

        return $html;
    }

    protected function get_include_contents($filename)
    {
        if (is_file($filename)) {
            ob_start();
            include $filename;
            return ob_get_clean();
        }
        return false;
    }

    protected function qSelect($field, $id)
    {
        $class = $this->get('class', $field) ? ' class="' . $field->class . '"' : '';
        $required = $this->get('required', $field) ? ' required' : '';
        $custom = $this->get('custom', $field) ? ' ' . $field->custom : '';
        $options = $this->get('options', $field) ? $field->options : false;
        $field->fildid = $id . '.' . $field->fildnum;

        $html = '';
        $html .= '<div class="qf2 qfselect' . ($required ? ' req' : '') .$this->boxClass($field). '">';
        $html .= $this->getLabel($field);

        $html .= '<select name="qfselect[]"' . $class . $custom . $required . ' data-settings="' . htmlentities('{"fildid":"' . $field->fildid . '"}') . '">';

        $i = '';
        foreach ($options as $option) {
            $related = $this->get('related', $option) ? $option->related : false;
            $calculator = (isset($option->calculator) && $option->calculator !== '') ? $option->calculator : '';

            $html .= '<option value="' . $i . '"';

            if($related && $calculator !== ''){
              $html .= ' data-settings="' . htmlentities('{"related":"' . $related . '","calculator":"' . $calculator . '"}') . '"';
            }
            elseif($related){
              $html .= ' data-settings="' . htmlentities('{"related":"' . $related . '"}') . '"';
            }
            elseif($calculator !== ''){
              $html .= ' data-settings="' . htmlentities('{"calculator":"' . $calculator . '"}') . '"';
            }

            $html .= '>' . $this->mlangLabel($option->label) . '</option>';
            $i ++;
        }

        $html .= '</select>';
        $html .= '</div>';

        return $html;
    }

    protected function qTabs($field, $id)
    {
        $class = $this->get('class', $field) ? ' ' . $field->class : '';
        $options = $this->get('options', $field) ? $field->options : false;
        $orient = $this->get('orient', $field) ? ' vertically' : ' horizontally';
        $field->fildid = $id . '.' . $field->fildnum;

        $html = '';

        if($field->label){
          $html .= '<div class="qftabslabel">' . $this->mlangLabel($field->label) . '</div>';
        }
        $html .= '<div class="qftabs' . $orient . $class . '">';

            $html .= '<div class="qftabslabelsbox">';
            $i = 0;
            foreach ($options as $option) {
                $additionalclass = ($i%2)?' qfodd':' qfeven';
                $activ = ($i==0)?' qftabactiv':'';

                $html .= '<div class="qftabsitemlabel'.$additionalclass.$activ.'">' . $this->mlangLabel($option->label) . '</div>';
                $i ++;
            }
            $html .= '</div>';


            $i = 0;
            foreach ($options as $option) {
                $related = $this->get('related', $option) ? $option->related : false;
                $display = ($i==0)?'':' style="display:none"';

                $html .= '<div class="qftabsitem"'.$display.'>';

                if ($related) {
                  $data = $this->getDataById($related);
                  if (!empty($data)) {
                      $html .= $this->getFilds($data);
                  }
                }

                $html .= '</div>';
                $i ++;
            }

        $html .= '</div>';

        return $html;
    }


    protected function qRadio($field, $id)
    {
        $html = '';
        $orient = $this->get('orient', $field) ? ' vertically' : ' horizontally';
        $custom = $this->get('custom', $field) ? ' ' . $field->custom : '';
        $class = $this->get('class', $field) ? $field->class : '';

        $html .= '<div class="qf2 qfradio' . $orient . $this->boxClass($field) . '">';
        $html .= $this->getLabel($field);
        $html .= '<div class="radioblok' . ($class ? ' radioblok_'.$class : '') . '">';

        $i = 0;
        $name = str_replace('.', '', 'r_' . microtime(1) . $field->fildnum);

        foreach ($field->options as $option) {

          $related = $this->get('related', $option) ? $option->related : false;
          $calculator = (isset($option->calculator) && $option->calculator !== '') ? $option->calculator : '';

          $html .= ($class ? '<span class="img_' . $class.'_'. $i . '"></span>' : '') .'<input type="radio" id="' . $name . $i . '" name="' . $name . '"' . ($class ? ' class="' . $class.'_'. $i .' '.$class . '"' : '') . $custom . ' value="' . $i . '"' . (! $i ? ' checked' : '');

          if($related && $calculator !== ''){
            $html .= ' data-settings="' . htmlentities('{"related":"' . $related . '","calculator":"' . $calculator . '"}') . '"';
          }
          elseif($related){
            $html .= ' data-settings="' . htmlentities('{"related":"' . $related . '"}') . '"';
          }
          elseif($calculator !== ''){
            $html .= ' data-settings="' . htmlentities('{"calculator":"' . $calculator . '"}') . '"';
          }

          $html .= ' /><label for="' . $name . $i . '">' . $this->mlangLabel($option->label) . '</label>';
          $i ++;
        }

        $html .= '<input name="qfradio[]" type="hidden" value="0" />';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    protected function qFile($field, $id)
    {
        $class = $this->get('class', $field) ? ' class="' . $field->class . '"' : '';
        $required = $this->get('required', $field) ? ' required' : '';
        $custom = $this->get('custom', $field) ? ' ' . $field->custom : '';

        $field->fildid = $id . '.' . $field->fildnum;

        $rand = str_replace('.', '', 'f_' . microtime(1) . $field->fildid);

        $html = '';
        $html .= '<div class="qf2 qffile' . ($required ? ' req' : '') . $this->boxClass($field) . '">';
        $html .= $this->getLabel($field);

        $html .= '<input type="file" id="' . $rand . '" name="inpfile[]"' . $class . $custom . $required . ' value="" /><label class="filelabel" for="' . $rand . '">' . $this->mlangLabel($field->label) . ($required ? ' *' : '') . '</label>';
        $html .= '<input name="qffile[]" type="hidden" value="0" />';

        $html .= '</div>';

        return $html;
    }

    protected function qCheckbox($field, $id)
    {
      $class = $this->get('class', $field) ? ' class="' . $field->class . '"' : '';
      $required = $this->get('required', $field) ? ' required' : '';
      $custom = $this->get('custom', $field) ? ' ' . $field->custom : '';
      $checked = $this->get('checked', $field) ? ' checked' : '';
      $related = $this->get('related', $field) ? $field->related : false;
      $calculator = (isset($field->calculator) && $field->calculator !== '') ? $field->calculator : '';
      $pos = $this->get('pos', $field) ? 1 : 0;
      $field->fildid = $id . '.' . $field->fildnum;

      $rand = str_replace('.', '', 'f_' . microtime(1) . $field->fildid);

      $html = '';
      $html .= '<div class="qf2 qfcheckbox' . ($required ? ' req' : '') . $this->boxClass($field) . '">';
      if($pos){
        $html .= '<label class="qf2label"></label>';
      }
      else{
        $html .= $this->getLabel($field);
      }

      $html .= '<input id="' . $rand . '" type="checkbox" name="chbx"' . $class . $custom . $checked . $required . ' value=""';

      if($related && $calculator !== ''){
        $html .= ' data-settings="' . htmlentities('{"related":"' . $related . '","calculator":"' . $calculator . '","fildid":"' . $field->fildid . '"}') . '"';
      }
      elseif($related){
        $html .= ' data-settings="' . htmlentities('{"related":"' . $related . '","fildid":"' . $field->fildid . '"}') . '"';
      }
      elseif($calculator !== ''){
        $html .= ' data-settings="' . htmlentities('{"calculator":"' . $calculator . '","fildid":"' . $field->fildid . '"}') . '"';
      }

      if($pos){
        $html .= ' /><label for="' . $rand . '" class="chbxlabel2">' . $this->mlangLabel($field->label) . ($required ? ' <span class="qf2labelreq">*</span>' : '') . '</label>';
      }
      else{
        $html .= ' /><label for="' . $rand . '" class="chbxlabel">' . $this->mlangLabel($field->label) . ($required ? ' <span class="qf2labelreq">*</span>' : '') . '</label>';
      }


      $html .= '<input name="qfcheckbox[]" type="hidden" value="0" />';
      $html .= '</div>';

      return $html;
    }

    protected function qInput($field, $id)
    {
        $class = $this->get('class', $field) ? ' class="' . $field->class . '"' : '';
        $required = $this->get('required', $field) ? ' required' : '';
        $custom = $this->get('custom', $field) ? ' ' . $field->custom : '';
        $placeholder = $this->get('placeholder', $field) ? ' placeholder="' . $this->mlangLabel($field->placeholder) . '"' : '';
        $value = $this->get('value', $field) ? ' value="' . $this->mlangLabel($field->value) . '"' : '';
        $calculator = (isset($field->calculator) && $field->calculator !== '') ? $field->calculator : '';

        $type = str_replace(array(
                'input[',
                ']'
        ), '', $field->teg);

        $field->fildid = $id . '.' . $field->fildnum;

        $html = '';
        $html .= '<div class="qf2 qf' . $type . ($required ? ' req' : '') . $this->boxClass($field) . '">';
        $html .= $this->getLabel($field);

        $html .= '<input type="' . $type . '" name="qf' . $type . '[]"' . $class . $custom . $required . $placeholder . $value;

        if($calculator !== ''){
          $html .= ' data-settings="' . htmlentities('{"calculator":"' . $calculator . '","fildid":"' . $field->fildid . '"}') . '"';
        }

        $html .= ' />';
        $html .= '</div>';

        return $html;
    }

    protected function qTextarea($field, $id)
    {
      $class = $this->get('class', $field) ? ' class="' . $field->class . '"' : '';
      $required = $this->get('required', $field) ? ' required' : '';
      $custom = $this->get('custom', $field) ? ' ' . $field->custom : '';
      $placeholder = $this->get('placeholder', $field) ? ' placeholder="' . $this->mlangLabel($field->placeholder) . '"' : '';

      $html = '';
      $html .= '<div class="qf2 qftextarea' . ($required ? ' req' : '') . $this->boxClass($field) . '">';
      $html .= $this->getLabel($field);
      $html .= '<textarea name="qftextarea[]"' . $class . $custom . $placeholder . $required . '></textarea>';
      $html .= '</div>';

      return $html;
    }

    protected function qSubmit($field, $id)
    {
      $class = $this->get('class', $field) ? ' class="' . $field->class . '"' : '';
      // $boxclass = $this->get('class', $field) ? ' box_' . $field->class : '';
      $custom = $this->get('custom', $field) ? ' ' . $field->custom : '';
      if(!isset($field->value)) $field->value = 'QF_SUBMIT';
      $value = $this->get('value', $field) ? ' value="' . $this->mlangLabel($field->value) . '"' : '';

      $html = '';
      $html .= '<div class="qf2 qfsubmit' . $this->boxClass($field) . '">';
      $html .= $this->getLabel($field);

      $html .= '<input name="qfsubmit" type="button"' . $class . $custom . $value . ' onclick="this.form.submit()" />';

      $html .= '</div>';

      return $html;
    }

    protected function boxClass($field) {
      if($this->get('class', $field)) {
        $arr = explode(' ', $field->class);
        foreach($arr as $k=>$v) {
          $arr[$k] = ' box_'.$v;
        }
        return implode('', $arr);
      }
      return '';
    }

    protected function qCustomHtml($field, $id)
    {
        $hideform = $this->get('hideform', $field) ? 1 : 0;
        if (! $hideform) {
            return html_entity_decode($this->mlangLabel($field->label));
        }
    }

    protected function qCalculatorSum($field, $id)
    {
        $html = '';
        $pos = $this->get('pos', $field) ? 1 : 0;
        $unit = $this->get('unit', $field) ? $this->mlangLabel($field->unit) : '';

        $html .= '<div class="qf2 qfprice">';
        $html .= $this->getLabel($field);
        if ($pos) {
            $html .= '<span class="qfpriceinner">0</span><span class="qfunitinner">' . $unit . '</span>';
        } else {
            $html .= '<span class="qfunitinner">' . $unit . '</span><span class="qfpriceinner">0</span>';
        }
        $html .= '<input name="qfprice[]" type="hidden" value="0" data-unit="'.$unit.'" />';
        $html .= '</div>';

        return $html;
    }

    protected function qRecaptcha($field, $id)
    {
        $html = '';
        $onlyguest = $this->get('show', $field) ? 1 : 0;
        $user = JFactory::getUser();

        if ($user->get('guest') || ! $onlyguest) {
            $captchaplugin = JPluginHelper::getPlugin('captcha', 'recaptcha');
            $captchaparams = new JRegistry();
            $captchaparams->loadString($captchaplugin->params);
            $pubkey = $captchaparams->get('public_key', '');
            $theme = $captchaparams->get('theme2', 'light');
            if (! $pubkey) {
                $html .= JText::_('PLG_RECAPTCHA_ERROR_NO_PUBLIC_KEY');
            } else {
                $html .= '<div class="qf2 qfcaptcha"><label class="qf2label"></label><div class="qf_recaptcha" data-sitekey="' . $pubkey . '" data-theme="' . $theme . '"></div></div>';
            }
        }

        return $html;
    }

    protected function qBackemail($field, $id)
    {
      $custom = $this->get('custom', $field) ? ' ' . $field->custom : '';
      $reg = $this->get('reg', $field) ? 1 : 0;

        $html = '';
        if ($reg) {
            $user = JFactory::getUser();
            if ($user->get('guest') != 1) {
                $html .= '<div class="qf2 qfbackemail">';
                $html .= $this->getLabel($field);
                $html .= '<input name="qfbackemail" type="checkbox" value="1"' . $custom . ' />';
                $html .= '</div>';
            }
        } else {
            $html .= '<div class="qf2 qfbackemail">';
            $html .= $this->getLabel($field);
            $html .= '<input name="qfbackemail" type="checkbox" value="1"' . $custom . ' />';
            $html .= '</div>';
        }

        return $html;
    }

    protected function qCloner($field, $id)
    {
      $orient = $this->get('orient', $field) ? ' vertically' : ' horizontally';
      $sum = $this->get('sum', $field) ? 1 : 0;
      $max = $this->get('max', $field) ? (int) $field->max : '';
      $related = $this->get('related', $field) ? (int) $field->related : 0;
      $clonerstart = $this->get('clonerstart', $field) ? $field->clonerstart : '';
      $clonerend = $this->get('clonerend', $field) ? $field->clonerend : '';
      $field->fildid = $id . '.' . $field->fildnum;

      $html = '';

        if (! $related) {
            return 'cloner not found';
        }
        if ($related == $id) {
            return 'error recursion error';
        }

        $data = $this->getDataById($related);
        if (empty($data)) {
            return '';
        }

        $html .= '<div class="qfcloner' . $orient . '" data-settings="' . htmlentities('{"sum":"' . $sum . '","max":"' . $max . '","related":"' . $related . '","fildid":"' . $field->fildid . '"}') . '">';
        $html .= '<input type="hidden" name= "qfcloner[]" value="1" data-settings="' . htmlentities('{"calculator":"' . $clonerstart . '"}') . '" />';
        $html .= '<div class="qfclonerrow">';
        $html .= $this->getFilds($data);
        if ($sum) {
            $html .= '<div class="qf2 qfclonesum"><label class="qf2label"> </label><span>0</span></div>';
        }
        $html .= '<div class="qf2 qfadd"><label class="qf2label"></label><a href="javascript:void(0)">+</a></div>';
        $html .= '<div class="qf2 qfrem"><label class="qf2label"></label><a href="javascript:void(0)">×</a></div>';
        $html .= '</div>';
        $html .= '<input type="hidden" data-settings="' . htmlentities('{"calculator":"' . $clonerend . '"}') . '" />';
        $html .= '</div>';

        return $html;
    }


    protected function qCalcCondition($field, $id)
    {
      $related = $this->get('related', $field) ? (int) $field->related : 0;
      $clonerstart = $this->get('clonerstart', $field) ? $field->clonerstart : '';
      $clonerend = $this->get('clonerend', $field) ? $field->clonerend : '';
      $condition = $this->get('condition', $field) ? $field->condition : '';

        $html = '';

        if (! $related) {
            return 'calcCondition not found';
        }
        if ($related == $id) {
            return 'error recursion error';
        }

        $data = $this->getDataById($related);
        if (empty($data)) {
            return '';
        }

        $html .= '<div class="calcCondition" data-settings="' . htmlentities('{"condition":"' . $condition . '","istrue":""}') . '">';
        $html .= '<input type="hidden" data-settings="' . htmlentities('{"calculator":"' . $clonerstart . '","cond":"1"}') . '" />';
        $html .= $this->getFilds($data);

        $html .= '<input type="hidden" data-settings="' . htmlentities('{"calculator":"' . $clonerend . '","cond":"1"}') . '" />';
        $html .= '</div>';

        return $html;
    }


    protected function qAddToCart($field, $id)
    {
      $class = $this->get('class', $field) ? ' class="' . $field->class . '"' : '';
      $custom = $this->get('custom', $field) ? ' ' . $field->custom : '';
      if(!isset($field->value)) $field->value = 'QF_ADDTOCART';
      $value = $this->get('value', $field) ? ' value="' . $this->mlangLabel($field->value) . '"' : '';

      $html = '';
      $html .= '<div class="qf2 qfaddtocart' . $this->boxClass($field) . '">';
      $html .= $this->getLabel($field);

      $html .= '<input name="qfaddcart" type="button"' . $class . $custom . $value . ' onclick="return this.form.qfaddtocart()" />';

      $html .= '</div>';

      return $html;
    }


    protected function getQFlink()
    {
        $params = JComponentHelper::getParams('com_quickform');
        $cl = '';
        if ($params->get('display') == '2' && trim($params->get('cod'))) {
            return '<input name="qfcod" type="hidden" value="' . trim($params->get('cod')) . '" />';
        } elseif ($params->get('display') == '1') {
            $cl = ' nfl';
        }

        return '<input name="qfcod" type="hidden" value="" /><div class="qfcapt' . $cl . '"><a href="http://juice-lab.ru" target="_blank"></a></div>';
    }

    public function ajaxHTML($id)
    {
        $html = '';
        if ($id) {
            $form = $this->getDataById($id);
            if (! empty($form)) {
                $project = $this->getProjectById($form->projectid);
                $html .= $this->getFilds($form);
            }
        }
        return $html;
    }
}
